/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bruit;

//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import static java.lang.Math.sqrt;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
//import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import org.w3c.dom.NamedNodeMap;

/**
 *
 * @author ollivier
 */
public class Bruit {

    private String _oldGPX;

    public Bruit(String gpx) {
        _oldGPX = gpx;
    }

    public String noNoise() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(_oldGPX));
            Document doc = (Document) builder.parse(is);
            NodeList trkptList = doc.getElementsByTagName("trkpt");
            double lat1, lon1, lat2, lon2,d;
            int i=0;
            while(i<trkptList.getLength()-1) {
                Element point1 = (Element) trkptList.item(i);
                lat1 = Double.parseDouble(point1.getAttribute("lat"));
                lon1 = Double.parseDouble(point1.getAttribute("lon"));
                Element point2 = (Element) trkptList.item(i + 1);
                lat2 = Double.parseDouble(point2.getAttribute("lat"));
                lon2 = Double.parseDouble(point2.getAttribute("lon"));
                d= distance(lat1,lat2,lon1,lon2);
                if (d >11.5) {
                    System.out.println("Remove Point");
                    point2.getParentNode().removeChild(point2);
                    trkptList = doc.getElementsByTagName("trkpt");
                }
                else 
                {
                    System.out.println("ok");
                    i++;
                }
                

            }

            String result = getStringFromDocument(doc);

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private double convertRad(double val) {
        return (Math.PI * val) / 180;
    }

    private double distance(double lat1, double lat2, double lon1, double lon2) {
        double R = 6378000; //Rayon de la terre en mètre

        double lat_a = convertRad(lat1);
        double lon_a = convertRad(lon1);
        double lat_b = convertRad(lat2);
        double lon_b = convertRad(lon2);

        double d = R * (Math.PI / 2 - Math.asin(Math.sin(lat_b) * Math.sin(lat_a) + Math.cos(lon_b - lon_a) * Math.cos(lat_b) * Math.cos(lat_a)));
        return d;
    }

    public String getStringFromDocument(Document doc) {
        try {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return writer.toString();
        } catch (TransformerException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * @param args the command line arguments
     */
    //public static void main(String[] args) throws IOException {
      /*  Bruit b = new Bruit("<gpx version=\"1.0\"><trk><name>Test Segment</name><trkseg><trkpt lat=\"49.214167\" lon=\"-0.3687386\"></trkpt><trkpt lat=\"49.2141279\" lon=\"-0.3686103\"></trkpt><trkpt lat=\"49.21437003\" lon=\"-0.36891259\"></trkpt><trkpt lat=\"49.21437233\" lon=\"-0.36893262\"></trkpt><trkpt lat=\"49.21437185\" lon=\"-0.36890806\"></trkpt><trkpt lat=\"49.21436149\" lon=\"-0.36888108\"></trkpt><trkpt lat=\"49.2168885\" lon=\"-0.3712861\"></trkpt><trkpt lat=\"49.2143462\" lon=\"-0.36884035\"></trkpt><trkpt lat=\"49.21433344\" lon=\"-0.36876342\"></trkpt><trkpt lat=\"49.21432637\" lon=\"-0.36871961\"></trkpt><trkpt lat=\"49.21430391\" lon=\"-0.3686623\"></trkpt><trkpt lat=\"49.21430808\" lon=\"-0.36868613\"></trkpt><trkpt lat=\"49.21430838\" lon=\"-0.36866763\"></trkpt><trkpt lat=\"49.21424279\" lon=\"-0.36845139\"></trkpt><trkpt lat=\"49.21427965\" lon=\"-0.3683413\"></trkpt><trkpt lat=\"49.21429154\" lon=\"-0.36837424\"></trkpt><trkpt lat=\"49.21428366\" lon=\"-0.36840262\"></trkpt><trkpt lat=\"49.21423033\" lon=\"-0.36840775\"></trkpt><trkpt lat=\"49.21420473\" lon=\"-0.36839221\"></trkpt><trkpt lat=\"49.21420375\" lon=\"-0.36841498\"></trkpt><trkpt lat=\"49.2142062\" lon=\"-0.36843977\"></trkpt><trkpt lat=\"49.21420882\" lon=\"-0.36842477\"></trkpt><trkpt lat=\"49.2142182\" lon=\"-0.36843196\"></trkpt><trkpt lat=\"49.21421964\" lon=\"-0.36844677\"></trkpt><trkpt lat=\"49.21420684\" lon=\"-0.36843875\"></trkpt><trkpt lat=\"49.21419593\" lon=\"-0.36843908\"></trkpt><trkpt lat=\"49.21419204\" lon=\"-0.36845206\"></trkpt><trkpt lat=\"49.21420146\" lon=\"-0.36845698\"></trkpt><trkpt lat=\"49.2145645\" lon=\"-0.3681477\"></trkpt><trkpt lat=\"49.21419263\" lon=\"-0.36846159\"></trkpt><trkpt lat=\"49.21417932\" lon=\"-0.36847523\"></trkpt><trkpt lat=\"49.21415959\" lon=\"-0.36847512\"></trkpt><trkpt lat=\"49.21413431\" lon=\"-0.36846629\"></trkpt><trkpt lat=\"49.21411796\" lon=\"-0.36845632\"></trkpt><trkpt lat=\"49.21410442\" lon=\"-0.3684461\"></trkpt><trkpt lat=\"49.21408638\" lon=\"-0.3684358\"></trkpt><trkpt lat=\"49.21406038\" lon=\"-0.36842517\"></trkpt><trkpt lat=\"49.2140263\" lon=\"-0.36840748\"></trkpt><trkpt lat=\"49.21398554\" lon=\"-0.36838709\"></trkpt><trkpt lat=\"49.21391178\" lon=\"-0.36827694\"></trkpt><trkpt lat=\"49.21387335\" lon=\"-0.36825887\"></trkpt><trkpt lat=\"49.21383867\" lon=\"-0.36822416\"></trkpt><trkpt lat=\"49.21380822\" lon=\"-0.36813936\"></trkpt><trkpt lat=\"49.21377481\" lon=\"-0.36806183\"></trkpt><trkpt lat=\"49.21379743\" lon=\"-0.36801983\"></trkpt><trkpt lat=\"49.21380028\" lon=\"-0.36797873\"></trkpt><trkpt lat=\"49.2146042\" lon=\"-0.3681056\"></trkpt><trkpt lat=\"49.21381041\" lon=\"-0.3679174\"></trkpt><trkpt lat=\"49.21380692\" lon=\"-0.36783064\"></trkpt><trkpt lat=\"49.21382082\" lon=\"-0.36771334\"></trkpt><trkpt lat=\"49.21386419\" lon=\"-0.36761581\"></trkpt><trkpt lat=\"49.21385572\" lon=\"-0.36755215\"></trkpt><trkpt lat=\"49.21387349\" lon=\"-0.36750445\"></trkpt><trkpt lat=\"49.21386756\" lon=\"-0.36742688\"></trkpt><trkpt lat=\"49.21387652\" lon=\"-0.36734314\"></trkpt><trkpt lat=\"49.21390218\" lon=\"-0.36727506\"></trkpt><trkpt lat=\"49.21391222\" lon=\"-0.36724025\"></trkpt><trkpt lat=\"49.21391242\" lon=\"-0.367209\"></trkpt><trkpt lat=\"49.21392107\" lon=\"-0.36718476\"></trkpt><trkpt lat=\"49.21394249\" lon=\"-0.3671694\"></trkpt><trkpt lat=\"49.21396891\" lon=\"-0.36718045\"></trkpt><trkpt lat=\"49.21401118\" lon=\"-0.36717472\"></trkpt><trkpt lat=\"49.21404682\" lon=\"-0.36718947\"></trkpt><trkpt lat=\"49.21409623\" lon=\"-0.36718621\"></trkpt><trkpt lat=\"49.21414578\" lon=\"-0.36717779\"></trkpt><trkpt lat=\"49.21420074\" lon=\"-0.36718416\"></trkpt><trkpt lat=\"49.21423364\" lon=\"-0.36721322\"></trkpt><trkpt lat=\"49.21428111\" lon=\"-0.36721159\"></trkpt><trkpt lat=\"49.21426804\" lon=\"-0.36728503\"></trkpt><trkpt lat=\"49.2099416\" lon=\"-0.3664275\"></trkpt><trkpt lat=\"49.21426084\" lon=\"-0.36737472\"></trkpt><trkpt lat=\"49.21426806\" lon=\"-0.36740194\"></trkpt><trkpt lat=\"49.21427945\" lon=\"-0.36746153\"></trkpt><trkpt lat=\"49.21428831\" lon=\"-0.36747186\"></trkpt><trkpt lat=\"49.21429056\" lon=\"-0.36751903\"></trkpt><trkpt lat=\"49.21431619\" lon=\"-0.36759471\"></trkpt><trkpt lat=\"49.21432609\" lon=\"-0.36762865\"></trkpt><trkpt lat=\"49.21433717\" lon=\"-0.36767055\"></trkpt><trkpt lat=\"49.21434331\" lon=\"-0.36773478\"></trkpt><trkpt lat=\"49.21437238\" lon=\"-0.36784219\"></trkpt><trkpt lat=\"49.21440008\" lon=\"-0.36793546\"></trkpt><trkpt lat=\"49.21443686\" lon=\"-0.36804223\"></trkpt><trkpt lat=\"49.21449855\" lon=\"-0.36813928\"></trkpt><trkpt lat=\"49.21454469\" lon=\"-0.36822677\"></trkpt><trkpt lat=\"49.21455574\" lon=\"-0.36827609\"></trkpt><trkpt lat=\"49.2144073\" lon=\"-0.3678051\"></trkpt><trkpt lat=\"49.21456186\" lon=\"-0.36821752\"></trkpt><trkpt lat=\"49.21456187\" lon=\"-0.36826617\"></trkpt><trkpt lat=\"49.21454048\" lon=\"-0.36827945\"></trkpt><trkpt lat=\"49.21453819\" lon=\"-0.36829911\"></trkpt><trkpt lat=\"49.21454315\" lon=\"-0.36828302\"></trkpt><trkpt lat=\"49.21453307\" lon=\"-0.36827185\"></trkpt><trkpt lat=\"49.21450566\" lon=\"-0.36826935\"></trkpt><trkpt lat=\"49.21448679\" lon=\"-0.36827471\"></trkpt><trkpt lat=\"49.21444916\" lon=\"-0.36825388\"></trkpt><trkpt lat=\"49.21441742\" lon=\"-0.36824381\"></trkpt><trkpt lat=\"49.21438717\" lon=\"-0.36823616\"></trkpt><trkpt lat=\"49.2143672\" lon=\"-0.36821039\"></trkpt><trkpt lat=\"49.21434285\" lon=\"-0.36818268\"></trkpt><trkpt lat=\"49.21431998\" lon=\"-0.36815213\"></trkpt><trkpt lat=\"49.21430109\" lon=\"-0.36813666\"></trkpt><trkpt lat=\"49.21428779\" lon=\"-0.36813022\"></trkpt><trkpt lat=\"49.21429116\" lon=\"-0.36815586\"></trkpt><trkpt lat=\"49.21429103\" lon=\"-0.36817742\"></trkpt><trkpt lat=\"49.21429332\" lon=\"-0.36819173\"></trkpt><trkpt lat=\"49.21429374\" lon=\"-0.36820866\"></trkpt><trkpt lat=\"49.2142475\" lon=\"-0.3683139\"></trkpt><trkpt lat=\"49.21430059\" lon=\"-0.36822132\"></trkpt><trkpt lat=\"49.21428787\" lon=\"-0.36822811\"></trkpt><trkpt lat=\"49.21427559\" lon=\"-0.36823221\"></trkpt><trkpt lat=\"49.21426618\" lon=\"-0.36824114\"></trkpt><trkpt lat=\"49.21425979\" lon=\"-0.3682528\"></trkpt><trkpt lat=\"49.21424719\" lon=\"-0.3682726\"></trkpt><trkpt lat=\"49.21423801\" lon=\"-0.36829871\"></trkpt><trkpt lat=\"49.21422883\" lon=\"-0.36833179\"></trkpt><trkpt lat=\"49.214223\" lon=\"-0.36835785\"></trkpt><trkpt lat=\"49.21421847\" lon=\"-0.36837771\"></trkpt><trkpt lat=\"49.2142166\" lon=\"-0.36839512\"></trkpt><trkpt lat=\"49.2099416\" lon=\"-0.3664275\"></trkpt></trkseg></trk></gpx>");
        String s = b.noNoise();
        System.out.println(s);
         BufferedWriter bw = new BufferedWriter(new FileWriter("newVelo.gpx"));
        PrintWriter pWriter = new PrintWriter(bw);
        pWriter.print(s);
        pWriter.close();
*/
   // }

}
