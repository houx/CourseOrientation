# Application 'Course d'Orientation' 

L'application vise à faciliter la pratique de la course d'orientation.

L'application disposera de deux modes: course et famille.
Dans ces deux modes l'application permet de disposer de la carte en version numérique. Le passage aux balises est validé grâce au scan d'un QRcode ou à 
la lecture d'un tag NFC.

* Mode famille:
    * Pour la pratique de la course d'orientation amateur (pas de timer ou de trace GPS)
    
* Mode course:
    * Le temps entre chaque balise, le temps final ainsi que le tracé de la course sont enregistrés. Il est possible de les consulter sur le site : http://pyxida.vikazim.fr/appli/statistiques.php
    
* Toutes ces données, visant à améliorer la performance des sportifs, seront disponibles sur un serveur.



---------- 
## Tâches à faire : 
    
* Upload des cartes 
    * Il est possible d'upload des cartes à partir du site vers le serveur, mais l'application utilise encore l'ancien système de XML et 
        ne prend pas en compte le serveur. (Voir rapport pour précision sur fonctionnement actual). 
    * Il faut changer la façon dont l'application récupère les cartes (principalement les classes CreateMapFromXML et DownloadFileFromURL) 
        par des appels sur le serveur. (Documentation serveur : http://pyxida.vikazim.fr:8080/documentation )
        
* Postes manquants 
    * Bouton pour permettre d'avertir les organisateurs si un poste est endommagé ou manquant 
        (Idéalement en comparant la position courante et la position du poste pour vérifier)

* Permettre le zoom en dessous de 1 (Zoomer plus, utile lorsque la carte est grande)


    

----------


## IDE et langage 
Application développée en **JAVA** sur **Android Studio**


----------


## Auteurs
 - Proux Elodie -- *proux@ecole.ensicaen.fr*
 - Pellegrini Yann -- *ypellegrini@ecole.ensicaen.fr*
 - Houx Benjamin -- *houx@ecole.ensicaen.fr*
 - Ollivier Axel --  *ollivier@ecole.ensicaen.fr*
 - Duplessis Vincent - *duplessis@ecole.ensicaen.fr*

----------


## Licence 
**LGPL V3**


----------



![présentation](doc/presentation.png)