/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.utils.map;

import com.projet2a.projetcourseorientation.modele.raceComponents.Balise;

import java.util.ArrayList;


/**
 * Classe permettant d'utiliser le GPS et de calculer les infos le concernant.
 */

public class GPSCalculator {
    private float[] _positionBaliseStart;
    private float[] _positionBalise1;
    private double[] _distanceStartBalise1;
    private double[] _distanceXYPosStartBalise1;
    private double[] _distanceXYPosStartBalise1LongLat;
    private double[][] _arrayLongLat;
    private double[][] _arrayXPosYPos;


    /**
     * Constructeur du GPS calculator
     * @param balises
     * @param positionBaliseStart
     * @param positionBalise1
     */
    public GPSCalculator(ArrayList<Balise> balises, float[] positionBaliseStart, float[] positionBalise1) {
        _positionBaliseStart = positionBaliseStart;
        _positionBalise1 = positionBalise1;
        calculPosCarteByLongLat(balises);
    }

    /**
     * Calcul des positions sur la carte grace à la long/lat
     * @param balises
     */
    private void calculPosCarteByLongLat(ArrayList<Balise> balises){
        int nbBalises = balises.size();
        _arrayLongLat = new double[nbBalises][2];
        _arrayXPosYPos = new double[nbBalises][2];

        for(int i=0 ; i< nbBalises ;i++){
            _arrayLongLat[i] = balises.get(i).get_LongLat();
            _arrayXPosYPos[i] = balises.get(i).get_xyPos();
        }

        _distanceStartBalise1 = new double[2];
        _distanceStartBalise1[0] = _positionBalise1[0] - _positionBaliseStart[0];
        _distanceStartBalise1[1] = _positionBalise1[1] - _positionBaliseStart[1];

        _distanceXYPosStartBalise1 = new double[2];
        _distanceXYPosStartBalise1[0] = _arrayXPosYPos[1][0] - _arrayXPosYPos[0][0];
        _distanceXYPosStartBalise1[1] = _arrayXPosYPos[1][1] - _arrayXPosYPos[0][1];

        _distanceXYPosStartBalise1LongLat = new double[2];
        _distanceXYPosStartBalise1LongLat[0] = _arrayLongLat[1][0] - _arrayLongLat[0][0];
        _distanceXYPosStartBalise1LongLat[1] = _arrayLongLat[1][1] - _arrayLongLat[0][1];
    }

    /**
     * Calcule la position sur la carte à partir du numero de balise par rapport à la balise de reference
     * @param numBalise
     * @return position
     */
    public float[] calculPosPourCarte(int numBalise){
        float[] XYPosPourCarte = new float[2];
        int numBaliseDeRef=0;
        if (difValAbs(_arrayXPosYPos[numBalise][0],_arrayXPosYPos[numBaliseDeRef][0])<2){
            numBaliseDeRef=1;
            XYPosPourCarte[0]= (float)(_positionBalise1[0]+((_arrayXPosYPos[numBalise][0] - _arrayXPosYPos[numBaliseDeRef][0])*_distanceStartBalise1[0]/_distanceXYPosStartBalise1[0]));
            XYPosPourCarte[1]= (float)(_positionBalise1[1]+((_arrayXPosYPos[numBalise][1] - _arrayXPosYPos[numBaliseDeRef][1])*_distanceStartBalise1[1]/_distanceXYPosStartBalise1[1]));

        }
        else {
            XYPosPourCarte[0]= (float)(_positionBaliseStart[0]+((_arrayXPosYPos[numBalise][0] - _arrayXPosYPos[numBaliseDeRef][0])*_distanceStartBalise1[0]/_distanceXYPosStartBalise1[0]));
            XYPosPourCarte[1]= (float)(_positionBaliseStart[1]+((_arrayXPosYPos[numBalise][1] - _arrayXPosYPos[numBaliseDeRef][1])*_distanceStartBalise1[1]/_distanceXYPosStartBalise1[1]));
            }
        return XYPosPourCarte;
    }


    /**
     * Calcule la différence en valeur absolue
     * @param val1
     * @param val2
     * @return diff
     */
    private double difValAbs(double val1,double val2){
        double valAbs = val1 - val2;
        if (valAbs >0){
            return valAbs;
        }
        return -valAbs;
    }


}

