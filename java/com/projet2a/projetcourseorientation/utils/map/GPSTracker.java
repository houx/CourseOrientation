/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.utils.map;


import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.projet2a.projetcourseorientation.secondaryActivities.CourseActivity;
import com.projet2a.projetcourseorientation.utils.Bruit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


/**
 *  Classe servant a utiliser le GPS.
 */

public class GPSTracker extends Service implements LocationListener {

    private final Context _mContext;

    // flag for GPS status

    private boolean _isGPSEnabled = false;

    // flag for network status
    private boolean _isNetworkEnabled = false;

    // flag for GPS status
    private boolean _canGetLocation = false;

    private Location _location; // _location
    private double _latitude; // _latitude
    private double _longitude; // _longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000;

    // Declaring a Location Manager
    protected LocationManager _locationManager;

    //private FileWriter gpsTraceWriter;
    //private File file;

    private CourseActivity _courseActivity;
    private String _gpxString;

    /**
     * Constructeur du GPSTracker
     * @param context
     */
    public GPSTracker(Context context) {
        /*
        try {

            //File file = new File(context.getFilesDir(), "gpsTrace.data");
            file = new File(context.getExternalFilesDir(null), "gpsTrace.gpx");
            gpsTraceWriter = new FileWriter(file, true);
            gpsTraceWriter .write("<gpx version=\"1.0\">\n\r<trk><name>Test Segment</name><trkseg>");
            gpsTraceWriter.close();
            Log.d("dir", "created trace file");

        } catch (Exception e) {
            e.printStackTrace();
        }
        */
        _gpxString = "<gpx version=\"1.0\">\n\r<trk><name>Test Segment</name><trkseg>";
        this._mContext = context;
        getLocation();
    }

    /**
     * Getter de la localisation
     * @return localisation
     */
    public Location getLocation() {
        try {
            _locationManager = (LocationManager) _mContext.getSystemService(LOCATION_SERVICE);
            Log.d("getLocation", "getLocation method");
            // getting GPS status
            _isGPSEnabled = _locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            _isNetworkEnabled = _locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!_isGPSEnabled && !_isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this._canGetLocation = true;
                // First get _location from Network Provider
                if (_isNetworkEnabled) {
                    _locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (_locationManager != null) {
                        _location = _locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (_location != null) {
                            _latitude = _location.getLatitude();
                            _longitude = _location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services

                if (_isGPSEnabled) {
                    //if (_location == null) {
                    _locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("GPS Enabled", "GPS Enabled");
                    if (_locationManager != null) {
                        _location = _locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (_location != null) {
                            _latitude = _location.getLatitude();
                            _longitude = _location.getLongitude();
                        }
                    }
                }
                    //}
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return _location;
    }

    /**
     * Arrete l'emploi du GPS
     * */
    public void stopUsingGPS(){
        if(_locationManager != null) {
            _locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Getter de la latitude
     * */
    public double getLatitude(){
        if(_location != null){
            _latitude = _location.getLatitude();
        }

        // return _latitude
        return _latitude;
    }

    /**
     * Getter de la longitude
     * */
    public double getLongitude(){
        if(_location != null){
            _longitude = _location.getLongitude();
        }

        // return _longitude
        return _longitude;
    }

    /**
     * Verifie que le GPS/WIFI est active
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this._canGetLocation;
    }

    /**
     * Montre le dialogue des parametres du GPS
     * */
    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(_mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                _mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    /**
     * Utilisee quand on se deplace, recupère la position et la met dans le GPX
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        //_courseActivity.setPositionPointeur(location.getLongitude(),location.getLatitude());

        CharSequence text = "gps : log : " + location.getLongitude() + "lat : " + location.getLatitude();
        this._location = location;
        int duration = Toast.LENGTH_LONG;

/*
        try {
            gpsTraceWriter = new FileWriter(file, true);
            gpsTraceWriter .append("<trkpt lat=\"" + location.getLatitude() + "\" lon=\"" + location.getLongitude() + "\"></trkpt>");
            /* Possibilité d'ajouter le temps dans une balise <time> *\/
            gpsTraceWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
*/

        _gpxString += "<trkpt lat=\"" + location.getLatitude() + "\" lon=\"" + location.getLongitude() + "\"></trkpt>";

        //Toast toast = Toast.makeText(_mContext, text, duration);
        //toast.show();
    }

    /**
     * Fin de la course, fini le GPX
     * @return string
     */
    public String endRace() {
        System.err.println("ENDRACE!!!!");
        /* try {
            gpsTraceWriter = new FileWriter(file, true);
            gpsTraceWriter.append("</trkseg></trk></gpx>");
            gpsTraceWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } */

        _gpxString += "</trkseg></trk></gpx>";

        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), "trace.gpx");

        try {
            FileOutputStream f = new FileOutputStream(file);
            f.write(_gpxString.getBytes());

            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String ret = new Bruit(_gpxString).noNoise();
        
        String debruit = new Bruit((_gpxString)).noNoise();
        
        // Trace en local pour le debug dans le répertoire downloads du téléphone
        file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), "traceDebruite.gpx");

        try {
            FileOutputStream f = new FileOutputStream(file);
            f.write(debruit.getBytes());

            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // Remet un header du xml pour la prochaine trace (une seule instanciation de cette classe)
        _gpxString = "<gpx version=\"1.0\">\n" +
                "\n" +
                "<trk><name>Test Segment</name><trkseg>";

        return ret;
    }

    /**
     * Methode a override
     * @param provider
     */
    @Override
    public void onProviderDisabled(String provider) {
    }

    /**
     * Methode a override
     * @param provider
     */
    @Override
    public void onProviderEnabled(String provider) {
    }

    /**
     * Methode a override
     * @param provider
     * @param status
     * @param extras
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    /**
     * Methode a override renvoyant null
     * @param arg0
     * @return null
     */
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    /**
     * Setter de l'observer
     * @param courseActivity
     */
    public void setObserver(CourseActivity courseActivity){
        _courseActivity = courseActivity;
    }

}
