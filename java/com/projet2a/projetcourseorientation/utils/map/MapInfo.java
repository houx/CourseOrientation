/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.utils.map;

import android.util.Log;
import android.widget.ImageView;


/**
 * Classe recueillant les infos sur la carte.
 */

public class MapInfo {
    private float _currentX;
    private float _currentY;
    private int _maxX;
    private int _maxY;
    private float _height;
    private float _width;
    private float _currentHeight;
    private float _currentWidth;
    private float _currentScale;
    private float _widthInitial;
    private float _heightInitial;

    /**
     * Constructeur du map info
     * @param img
     * @param maxX
     * @param maxY
     */
    public MapInfo(ImageView img, int maxX, int maxY){
        int[] img_coordinates = new int[2];

        img.getLocationOnScreen(img_coordinates);
        _height = img.getDrawable().getIntrinsicHeight();
        _width = img.getDrawable().getIntrinsicWidth();
        _currentX = img_coordinates[0];
        _currentY = img_coordinates[1];
        _maxX = maxX;
        _maxY = maxY;
        _currentHeight = _height;
        _currentWidth = _width;
        _widthInitial = _width;
        _heightInitial = _height;
        _currentScale = 1.0f;



    }


    /**
     * MaJ de la position
     * @param x
     * @param y
     */
    public void updatePosition(float x, float y){
        _currentX = _currentX + x ;
        _currentY = _currentY + y ;
    }

    /**
     * MaJ de la position sur la carte
     * @param x
     * @param y
     * @return pos
     */
    public int[] updateXY(int x, int y){

        int[] out = new int[2];
        out[0] = x ;
        out[1] = y;

        if (_currentX  + _currentWidth > _maxX && x > 0 && _currentX > -1)
            out[0]=0;
        if (_currentY +120  + _currentHeight > _maxY && y > 0 && _currentY >-1)
            out[1]=0;

        if (_currentX  + _currentWidth < _maxX && x < 0 && _currentX < -1)
            out[0]=0;
        if (_currentY +122  + _currentHeight < _maxY && y < 0 && _currentY < -1)
            out[1]=0;

        return out;
    }

    /**
     * toString classique
     * @return string
     */
    public String toString(){

        return "mX:" + _maxX + "mY"+ _maxY  +"CH:" + (int)(_currentHeight) + "CW" + (int)(_currentWidth) +"Y" + (int)_currentY + "X"+(int)_currentX;
    }

    /**
     * MaJ de l'echelle
     * @param f
     */
    public void updateScale(float f){
        _currentHeight =  _height* f;
        _currentWidth = _width * f;

        _currentScale  = _currentHeight/_heightInitial;
        //Log.e("Scale",String.valueOf(_currentScale));

    }

    /**
     * Recupere l'echelle
     * @return currentscale
     */
    public float get_currentScale(){
        return _currentScale;
    }
}