/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.utils.map;

import android.util.Log;
import android.widget.ImageView;


/**
 * Classe permettant de traiter la carte, le deplacement, le zoom.
 */

public class PointersPositionControler {
    private ImageView _iv;
    private float _positionX;
    private float _positionY;
    private int _positionXScale;
    private int _positionYScale;
    private int _deplacementX=0;
    private int _deplacementY=0;
    private float _scale=1.0f;
    private int _initDepX=0;
    private int _initDepY=0;
    private int _height;
    private int _width;

    //Y heigth,X Width

    /**
     * Constructeur du Controler
     * @param iv
     * @param positionX
     * @param positionY
     * @param centrage
     */
    public PointersPositionControler(ImageView iv, int positionX, int positionY, boolean centrage){
        _iv = iv;
        _positionX = positionX;
        _positionY = positionY;
        _positionXScale = positionX;
        _positionYScale = positionY;
        _iv.setAlpha(0.5f);
        _height = _iv.getDrawable().getIntrinsicHeight();
        _width = _iv.getDrawable().getIntrinsicWidth();
        if(centrage){
            centrage();
        }
    }

    /**
     * MaJ de l'echelle
     * @param scale
     */
    public void updateScale(float scale){
        float positionXScaleTmp = _positionX * scale;
        float positionYScaleTmp = _positionY * scale;
        int valScrollX = 0;
        int valScrollY = 0;
        _scale = scale;

        if ( difValAbs(positionXScaleTmp,(float)_positionXScale) > 1.0f ){
            valScrollX = _positionXScale - (int)positionXScaleTmp;
            _positionXScale = _positionXScale - valScrollX;
        }

        if ( difValAbs(positionYScaleTmp,(float)_positionYScale) > 1.0f ){
            valScrollY = _positionYScale - (int)positionYScaleTmp;
            _positionYScale = _positionYScale - valScrollY;
        }
        doScroll(valScrollX, valScrollY);
    }

    /**
     * Permet de déplacer la carte
     * @param XVal
     * @param YVal
     */
    public void doDeplacement(int XVal,int YVal){
        _positionYScale += YVal;
        _positionXScale += XVal;
        _positionX += XVal/_scale;
        _positionY += YVal/_scale;
        doScroll(-XVal, -YVal);
    }

    /**
     * Initialise le fait de pouvoir se deplacer
     * @param XVal
     * @param YVal
     */

    public void doInitDeplacement(int XVal,int YVal){

        int[] img_coordinates = new int[2];
        _iv.getLocationOnScreen(img_coordinates);
        _initDepX = img_coordinates[0];
        _initDepY = img_coordinates[1];


        doScroll(_positionXScale, _positionYScale);
        _positionYScale = YVal-_initDepY-_deplacementY;
        _positionXScale = XVal-_initDepX-_deplacementX;
        _positionY = ((YVal-_initDepY-_deplacementY)/_scale);
        _positionX = ((XVal-_initDepX-_deplacementX)/_scale);
        doScroll(_deplacementX + _initDepX - XVal, _deplacementY + _initDepY - YVal);
    }

    /**
     * Calcule la difference en valeur absolue
     * @param positionScaleTmp
     * @param positionScale
     * @return
     */

    private float difValAbs(float positionScaleTmp,float positionScale){
        float valAbs = positionScaleTmp - positionScale;
        if (valAbs >0){
            return valAbs;
        }
        return -valAbs;
    }

    public void doScroll(int XVal,int YVal){
        _iv.scrollBy(XVal,YVal);
    }

    private void centrage(){
        _iv.scrollBy(_width/2,_height/2);
    }

    /**
     * MaJ le deplacement
     * @param XVal
     * @param YVal
     */
    public void updateDeplacementMap(int XVal, int YVal) {
        _deplacementX+=XVal;
        _deplacementY+=YVal;
    }

    /**
     * Renvoie la position
     * @return pos
     */
    public float[] giveTruePosition(){
        float[] pos = new float[2];
        pos[0] = _positionX;
        pos[1] = _positionY;
        return pos;
    }

    /**
     * MaJ la position
     * @param XVal
     * @param YVal
     */

    public void updatePosition(float XVal,float YVal){
        int[] img_coordinates = new int[2];
        _iv.getLocationOnScreen(img_coordinates);
        _initDepX = img_coordinates[0];
        _initDepY = img_coordinates[1];
        doScroll(_positionXScale, _positionYScale);
        _positionY = YVal;
        _positionX = XVal;
        _positionYScale = (int)(YVal*_scale);
        _positionXScale = (int)(XVal*_scale);

        Log.d("pointer", "pointeur position update");
        doScroll(-_positionXScale, -_positionYScale);
    }
}
