/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */


package com.projet2a.projetcourseorientation.utils;


import android.content.Context;
import com.projet2a.projetcourseorientation.R;
import com.projet2a.projetcourseorientation.modele.raceComponents.Carte;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.SAXException;

import com.projet2a.projetcourseorientation.modele.raceComponents.Balise;
import com.projet2a.projetcourseorientation.modele.raceComponents.Carte;


/**
 * Classe utilisee lors du telechargement de la carte, elle permet d'initialiser les donnees d'une course.
 */


public class CreateMapFromXML
{

    private int _nbCourses = 0;
    private String _xml;

    /**
     * Constructeur de la classe
     * @param xml
     */
    public CreateMapFromXML(String xml) {
        _xml = xml;
    }

    /**
     * Cree la carte depuis le XML
     * @return carte
     */
    public ArrayList<Carte> createArrayMap(){
        File fileXMLs = new File(_xml);
        Carte[] arrayCartes = null;
        final DocumentBuilderFactory factorys = DocumentBuilderFactory.newInstance();
        int PPPP =0;
        ArrayList<Carte> _cartes = new ArrayList<>();



        try {
            final DocumentBuilder builder = factorys.newDocumentBuilder();
            final Document document = builder.parse(fileXMLs);

            Map<String,Balise> mapBalise = new HashMap<>();

            NodeList datas = document.getElementsByTagName("RaceCourseData");

            for (int w = 0; w < datas.getLength(); w++) {
                /* debuggg!!!!
                String test="PP";
                test += datas.item(i).getChildNodes().getLength();
                NodeList originChild = datas.item(i).getChildNodes();
                for (int j =0 ; j < originChild.getLength() ; j++){
                    test += "[" + originChild.item(j).getNodeName() + '|';
                }
                 return test;
                */

                NodeList originChild = datas.item(w).getChildNodes();
                for (int i =0; i < originChild.getLength() ; i++) {
                    //test += "\n" + originChild.item(i).getNodeName();
                    if (originChild.item(i).getNodeName().equals("Map")) {
                        NodeList mapNodes = originChild.item(i).getChildNodes();
                        PPPP += 1;
                        for (int j = 0; j < mapNodes.getLength(); j++) {
                            if (mapNodes.item(j).getNodeName().equals("Scale")) {
                                String scale = mapNodes.item(j).getTextContent();
                                PPPP += 1;
                            }

                            if (mapNodes.item(j).getNodeName().equals("MapPositionTopLeft")) {
                                Element positionTopLeft = (Element) mapNodes.item(j);
                                String x = positionTopLeft.getAttribute("x");
                                String y = positionTopLeft.getAttribute("y");
                                String unit = positionTopLeft.getAttribute("unit");
                                PPPP += 1;
                            }

                            if (mapNodes.item(j).getNodeName().equals("MapPositionBottomRight")) {
                                Element positionBottomRight = (Element) mapNodes.item(j);
                                String x2 = positionBottomRight.getAttribute("x");
                                String y2 = positionBottomRight.getAttribute("y");
                                String unit2 = positionBottomRight.getAttribute("unit");
                                PPPP += 1;
                            }
                        }
                    }

                    //test += originChild.item(i).getNodeName();
                    //NodeList control =    ((Element)datas.item(w)).getElementsByTagName("Control");
                    //BalisesArray balisesArray = new BalisesArray(control.getLength());

                    int numberOfBalise = -1;
                    if (originChild.item(i).getNodeName().equals("Control"))
                    {

                        //for (int numbControl =0 ;  numbControl < control.getLength() ; numbControl++) {
                        //test +=  control.item(numbControl).getNodeName();


                        String id = "";
                        String longitude = "";
                        String latitude = "";
                        String x = "";
                        String y = "";
                        String unit = "";
                        NodeList controlNodes = originChild.item(i).getChildNodes();
                        NodeList test3;
                        Node test4;
                        for (int k=0; k<controlNodes.getLength(); k++)
                        {
                            //test +=  controlNodes.item(k).getNodeName();
                            // NodeList Id =((Element)controlNodes.item(k)).getElementsByTagName("Control");
                            if (controlNodes.item(k).getNodeName().equals("Id"))
                            {id = controlNodes.item(k).getTextContent();}

                            if (controlNodes.item(k).getNodeName().equals("Position"))
                            {
                                Element position = (Element)controlNodes.item(k);
                                longitude = position.getAttribute("lng");
                                latitude = position.getAttribute("lat");
                            }
                            if (controlNodes.item(k).getNodeName().equals("MapPosition"))
                            {
                                Element positionBottomRight = (Element)controlNodes.item(k);
                                x = positionBottomRight.getAttribute("x");
                                y = positionBottomRight.getAttribute("y");
                                unit = positionBottomRight.getAttribute("unit");
                            }
                        }
                        //test +=  numbControl +'a'+ id+'a'+ latitude+'a'+longitude+'a'+ unit;
                        //Double a = Double.parseDouble(latitude);
                        //public Balise(int id, String content, double lat, double lng, String unit)
                        // Position pos = new Position(Double.parseDouble(x), Double.parseDouble(y));
                        Balise b = new Balise(++numberOfBalise, id, Double.parseDouble(latitude), Double.parseDouble(longitude), unit);
                        //balisesArray.addBalise(b);
                        //tabBalises.add(numberOfBalise, b);

                        //mapBalises.put(id, pos);
                        mapBalise.put(id,b);
                    }


                    // a tester
                    //test +=originChild.item(i).getNodeName();
                    //NodeList courses =    ((Element)datas.item(w)).getElementsByTagName("Course");
                    //for (int i = 0 ; i < courses.getLength();i++){
                    if (originChild.item(i).getNodeName().equals("Course")) {
                        NodeList courseNodes = originChild.item(i).getChildNodes();
                        String name = "";
                        String length = "";
                        String climb = "";
                        int nbBalises = 0;

                        //Map<Balise, Position> parcours = new HashMap<>();
                        ArrayList<Balise> tabBalises = new ArrayList();

                        for (int l=0; l<courseNodes.getLength(); l++)
                        {
                            //test += courseNodes.item(l).getTextContent();

                            if (courseNodes.item(l).getNodeName().equals("Name"))
                            {
                                name = courseNodes.item(l).getTextContent();
                            }

                            if (courseNodes.item(l).getNodeName().equals("Length"))
                            {
                                length = courseNodes.item(l).getTextContent();
                            }

                            if (courseNodes.item(l).getNodeName().equals("Climb"))
                            {
                                climb = courseNodes.item(l).getTextContent();
                            }
                            //
                            if (courseNodes.item(l).getNodeName().equals("CourseControl"))
                            {
                                NodeList courseControlNodes = courseNodes.item(l).getChildNodes();
                                String idBalise = "";
                                String distancePreviousBalise = "";
                                String typeBalise = "";
                                for (int m=0; m<courseControlNodes.getLength(); m++)
                                {
                                    //Element type = (Element)courseControlNodes.item(m);
                                    //typeBalise = type.getAttribute("type");


                                    if (courseControlNodes.item(m).getNodeName().equals("Control"))
                                    {
                                        idBalise = courseControlNodes.item(m).getTextContent();
                                    }

                                    if (courseControlNodes.item(m).getNodeName().equals("LegLength"))
                                    {
                                        distancePreviousBalise = courseControlNodes.item(m).getTextContent();
                                    }



                                }


                                // a tester
                                if(mapBalise.containsKey(idBalise))
                                {
                                    //Position posi = mapBalise.get(idBalise);
                                    Balise ba = mapBalise.get(idBalise);
                                    //parcours.put(ba, posi);
                                    tabBalises.add(nbBalises,ba);
                                    nbBalises++;
                                }

                            }//

                        }

                        Carte map = new Carte(name,tabBalises);
                        _cartes.add(_nbCourses,map);
                        _nbCourses++;

                    }
                    //}
                }


            }
        }catch (final ParserConfigurationException e)
        {
            e.printStackTrace();
        }

        catch (final SAXException e)
        {
            e.printStackTrace();
        }

        catch (final IOException e)
        {
            e.printStackTrace();
        }
        return _cartes;
    }

    /**
     * Cree le lien de telechargement de la carte
     * @return lien
     */
    public Map<String,String> createMapLienDownload(){
        Map<String,String> mapHtml = new HashMap<>();
        File fileXMLs = new File(_xml);
        final DocumentBuilderFactory factorys = DocumentBuilderFactory.newInstance();
        try {
            final DocumentBuilder builder = factorys.newDocumentBuilder();
            final Document document = builder.parse(fileXMLs);
            NodeList datas = document.getElementsByTagName("CourseOrientation");

            for (int w = 0; w < datas.getLength(); w++) {
                NodeList originChild = datas.item(w).getChildNodes();
                for (int i =0; i < originChild.getLength() ; i++) {
                    if (originChild.item(i).getNodeName().equals("infoMap"))
                    {
                        mapHtml.put("infoMap",originChild.item(i).getTextContent());
                    }
                    if (originChild.item(i).getNodeName().equals("map"))
                    {
                        NodeList map = originChild.item(i).getChildNodes();
                        String name = "";
                        String html = "";
                        for(int j=0;j<map.getLength();j++) {
                            if (map.item(j).getNodeName().equals("name")) {
                                name = map.item(j).getTextContent();
                            }
                            if (map.item(j).getNodeName().equals("html")) {
                                html = map.item(j).getTextContent();
                            }
                        }
                        mapHtml.put(name,html);
                    }
                }
            }

        }catch (final ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }
        //String test =mapHtml.get(0);
        return mapHtml;
    }

    /**
     * Cree le lien qrcode pour telecharger la carte
     * @return lien
     */
    public Map<String,String> createMapLienDownloadByQrcode(){
        Map<String,String> mapHtml = new HashMap<>();
        File fileXMLs = new File(_xml);
        final DocumentBuilderFactory factorys = DocumentBuilderFactory.newInstance();
        try {
            final DocumentBuilder builder = factorys.newDocumentBuilder();
            final Document document = builder.parse(fileXMLs);
            NodeList datas = document.getElementsByTagName("CourseOrientation");

            for (int w = 0; w < datas.getLength(); w++) {
                NodeList originChild = datas.item(w).getChildNodes();
                for (int i =0; i < originChild.getLength() ; i++) {
                    if (originChild.item(i).getNodeName().equals("InformationGenerale"))
                    {
                        mapHtml.put("InformationGenerale",originChild.item(i).getTextContent());
                    }
                    if (originChild.item(i).getNodeName().equals("CarteATelecharger"))
                    {
                        NodeList map = originChild.item(i).getChildNodes();
                        String name = "";
                        String html = "";
                        for(int j=0;j<map.getLength();j++) {
                            if (map.item(j).getNodeName().equals("name")) {
                                name = map.item(j).getTextContent();
                            }
                            if (map.item(j).getNodeName().equals("html")) {
                                html = map.item(j).getTextContent();
                            }
                        }
                        mapHtml.put("CarteATelecharger",html);
                        mapHtml.put("nom",name);
                    }
                }
            }

        }catch (final ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }
        //String test =mapHtml.get(0);
        return mapHtml;
    }


    /*FONCTION DE TEST*/
    /*
    public String createMapLienDownloadTest(){
        Map<String,String> mapHtml = new HashMap<>();
        File fileXMLs = new File(_xml);
        final DocumentBuilderFactory factorys = DocumentBuilderFactory.newInstance();
        String str = "";
        try {
            final DocumentBuilder builder = factorys.newDocumentBuilder();
            final Document document = builder.parse(fileXMLs);
            NodeList datas = document.getElementsByTagName("CourseOrientation");

            for (int w = 0; w < datas.getLength(); w++) {

                NodeList originChild = datas.item(w).getChildNodes();
                for (int i =0; i < originChild.getLength() ; i++) {

                    if (originChild.item(i).getNodeName().equals("infoMap"))
                    {
                        mapHtml.put("infoMap",originChild.item(i).getTextContent());
                    }
                    if (originChild.item(i).getNodeName().equals("map"))
                    {
                        //str += originChild.item(i).getNodeName();
                        NodeList map = originChild.item(i).getChildNodes();
                        String name = "";
                        String html = "";
                        for(int j=0;j<map.getLength();j++) {

                            if (map.item(j).getNodeName().equals("name")) {
                                name = map.item(j).getTextContent();
                            }
                            if (map.item(j).getNodeName().equals("html")) {
                                html = map.item(j).getTextContent();
                            }
                        }
                        mapHtml.put(name,html);
                    }
                }
            }

        }catch (final ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }
        //String test =mapHtml.get(0);
        return str;
    }*/


    /**
     * Getter du nombre de courses
     * @return nbCourses
     */
    public int getNbCourses()
    {
        return _nbCourses;
    }
}