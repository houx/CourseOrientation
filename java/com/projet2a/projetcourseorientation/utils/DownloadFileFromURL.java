/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.utils;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;


/**
 * Classe utilisee pour le telechargement de fichiers XML/images via une URL.
 */

public class DownloadFileFromURL extends AsyncTask<String, String, String> {
    private boolean _chaineDDL;
    private String _dirName;
    private String _fileName = null;
    private ObserverDownloadMenu _vueSup=null;
    private boolean _ddlForMap;

    /**
     * Constructeur a partir du nom de repertoire
     * @param dirName
     */
    public DownloadFileFromURL(String dirName){
        _chaineDDL = false;
        _dirName = dirName;
        _ddlForMap = false;
    }

    /**
     * Constructeur a partir du nom et du booleen chaineDDL
     * @param chaineDDL
     * @param dirName
     */
    public DownloadFileFromURL(boolean chaineDDL, String dirName){
        _chaineDDL = chaineDDL;
        _dirName = dirName;
        _ddlForMap = false;
    }

    /**
     * Constructeur qui verifie en plus le DDL pour la carte
     * @param chaineDDL
     * @param dirName
     * @param ddlForMap
     */
    public DownloadFileFromURL(boolean chaineDDL, String dirName,boolean ddlForMap){
        _chaineDDL = chaineDDL;
        _dirName = dirName;
        _ddlForMap = ddlForMap;
    }

    /**
     * Permet d'effectuer le telechargement en tache de fond
     * @param f_url_dir_filename
     * @return null
     */
    @Override
    protected String doInBackground(String... f_url_dir_filename) {
        int count;
        Log.i("download DIR : ", _dirName);

        for (String s : f_url_dir_filename)
        Log.w("download NAME: ", s);


        try {
            URL url = new URL(f_url_dir_filename[0]);
            URLConnection conection = url.openConnection();
            conection.connect();
            int lenghtOfFile = conection.getContentLength();
            Log.d("download map url : ", url.toString());
            InputStream input = new BufferedInputStream(url.openStream(),8192);
            try {
                File dir = new File(_dirName
                        + "/" + f_url_dir_filename[1]);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
            }
            catch(Exception e){
                Log.w("creating file error", e.toString());
            }
            // Output stream
            OutputStream output;
            if (f_url_dir_filename[1] != ""){
                output= new FileOutputStream(_dirName
                        + "/" + f_url_dir_filename[1]
                        + "/" + f_url_dir_filename[2]);
            }
            else{
                output= new FileOutputStream(_dirName
                        + "/" + f_url_dir_filename[2]);
                _fileName = f_url_dir_filename[2];
            }


            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }


        return null;
    }

    /**
     * Effectue le telechargement
     * @param s
     */
    @Override
    protected void onPostExecute(String s) {

        if (_chaineDDL && _fileName != null && _ddlForMap) {
            String str3 = _dirName + '/' + _fileName;
            CreateMapFromXML test3 = new CreateMapFromXML(str3);
            Map<String, String> mapHtml = test3.createMapLienDownloadByQrcode();
            if (mapHtml.containsKey("InformationGenerale")) {
                String url = mapHtml.get("InformationGenerale");
                String filename = filenamenFromUrl(url);
                DownloadFileFromURL download = new DownloadFileFromURL(true, _dirName);
                if (_vueSup != null) {
                    download.addObserver(_vueSup);
                }
                download.execute(url, "", filename);
            }
            if (mapHtml.containsKey("CarteATelecharger")) {
                String url = mapHtml.get("CarteATelecharger");
                String filename = filenamenFromUrl(url);
                DownloadFileFromURL download = new DownloadFileFromURL(_dirName);
                if(_vueSup != null) {
                    download.addObserver(_vueSup);
                }
                download.execute(url, "map", filename);
            }

            if (_vueSup != null) {
                _vueSup.destroyFichierTMP();
            }
        }else if (_chaineDDL && _fileName != null) {
            String str3 = _dirName + '/' + _fileName;
            CreateMapFromXML test3 = new CreateMapFromXML(str3);
            Map<String, String> mapHtml = test3.createMapLienDownload();
            if (mapHtml.containsKey("infoMap")) {
                String url = mapHtml.get("infoMap");
                String filename = filenamenFromUrl(url);
                DownloadFileFromURL download = new DownloadFileFromURL(_dirName);
                if(_vueSup != null) {
                    download.addObserver(_vueSup);
                }
                download.execute(url, "infoMap", filename);

            }
        }

        if(_vueSup != null){
            _vueSup.displayMap();
        }
        super.onPostExecute(s);
    }

    //changement

    /**
     * Recupere le nom du fichier
     * @param url
     * @return nom
     */
    public String filenamenFromUrl(String url){
        String[] list = url.split("/");
        String nomDuFichier = list[list.length-1];
        return nomDuFichier;
    }

    /**
     * Permet d'ajouter la vue
     * @param vueSup
     */
    public void addObserver(ObserverDownloadMenu vueSup){
        _vueSup = vueSup;
    }
}

