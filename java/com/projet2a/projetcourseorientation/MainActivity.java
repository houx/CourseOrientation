/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.projet2a.projetcourseorientation.secondaryActivities.ChooseMapActivity;

/**
 * Classe utilisee des le debut du fonctionnement de l'application, on devra choisir le mode et l'affichage de l'application.
 */


public class MainActivity extends AppCompatActivity {
    public static final int MODE_OK = 1;
    public String _mode = "sportif";
    public String _affichage = "portrait";


    /**
     * Cree l'activite
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Permet d'appuyer sur un bouton
     * @param view
     */
    public void onRadioButtonClickedMode(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.radioCourse:
                if (checked)
                {
                    _mode = "sportif";
                }
                break;
            case R.id.radioBallade:
                if (checked)
                {
                    _mode = "famille";
                }
                break;
        }
    }

    /**
     * Change l'affichage
     * @param view
     */
    public void onRadioButtonClickedAffichage(View view){
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()){
            case R.id.radioButton_AffichagePaysage:
                if(checked)
                {
                    _affichage = "paysage";
                    }
                break;
            case R.id.radioButton_AffichagePortrait:
                if(checked)
                {
                    _affichage = "portrait";
                }
                break;
        }
    }


    /**
     * Permet de demarrer l'activite
     * @param v
     */
    public void valider(View v) {
        if (_mode == null || _affichage == null){
            Toast.makeText(getBaseContext(), "Veuillez configurer les parametres de la course", Toast.LENGTH_SHORT).show();
        }
        else{
            Intent intent = new Intent(getBaseContext(), ChooseMapActivity.class);
            intent.putExtra("modeAppli", _mode);
            intent.putExtra("affichageAppli", _affichage);
            startActivityForResult(intent, MODE_OK);
        }

    }
}