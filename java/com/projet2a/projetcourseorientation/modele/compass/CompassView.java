/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.modele.compass;



import android.content.Context;
import android.content.res.Resources;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import android.os.SystemClock;

import android.util.AttributeSet;

import android.view.View;

import com.projet2a.projetcourseorientation.R;


/**
 * Classe permettant d'afficher la boussole.
 */

public class CompassView extends View {
    //~--- fields -------------------------------------------------------------

    private float _northOrientation = 0;

    private Paint _circlePaint;
    private Paint _northPaint;
    private Path _trianglePath;
    //Délais entre chaque image
    private final int DELAY = 20;
    //Durée de l'animation
    private final int DURATION = 1000;
    private float _startNorthOrientation;
    private float _endNorthOrientation;
    //Heure de début de l’animation (ms)
    private long _startTime;
    //Pourcentage d'évolution de l'animation
    private float _perCent;
    //Temps courant
    private long _curTime;
    //Temps total depuis le début de l'animation
    private long _totalTime;
    private Runnable animationTask = new Runnable() {
        public void run() {
            _curTime = SystemClock.uptimeMillis();
            _totalTime = _curTime - _startTime;

            if (_totalTime > DURATION) {
                _northOrientation = _endNorthOrientation % 360;
                removeCallbacks(animationTask);
            } else {
                _perCent = ((float) _totalTime) / DURATION;

                // Animation plus réaliste de l'aiguille
                _perCent = (float) Math.sin(_perCent * 1.5);
                _perCent = Math.min(_perCent, 1);
                _northOrientation = _startNorthOrientation + _perCent * (_endNorthOrientation - _startNorthOrientation);
                postDelayed(this, DELAY);
            }

            // on demande à notre vue de se redessiner
            invalidate();
        }
    };

    //~--- constructors -------------------------------------------------------

    /**
     * Constructeur par defaut de la vue
     * @param context
     *
     */

    public CompassView(Context context) {
        super(context);
        initView();
    }

    /**
     * Constructeur utilise pour instancier la vue depuis sa declaration dans un fichier XML
     * @param context
     * @param attrs
     */
    //
    public CompassView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    /**
     * Constructeur utilise pour instancier la vue depuis sa declaration dans un fichier XML
     * @param context
     * @param attrs
     * @param defStyle
     *
     */

    public CompassView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    //~--- get methods --------------------------------------------------------



    /**
     * permet de recuperer l'orientation de la boussole
     * @return _northOrientation
     */
    public float getNorthOrientation() {
        return _northOrientation;
    }

    //~--- set methods --------------------------------------------------------

    // permet de changer l'orientation de la boussole

    /**
     * Permet de changer l'orientation de la boussole
     * @param rotation
     */
    public void setNorthOrientation(float rotation) {
        // on met à jour l'orientation uniquement si elle a changé
        if (rotation != this._northOrientation) {
            //Arrêter l'ancienne animation
            removeCallbacks(animationTask);

            //Position courante
            this._startNorthOrientation = this._northOrientation;
            //Position désirée
            this._endNorthOrientation = rotation;

            //Détermination du sens de rotation de l'aiguille
            if ( ((_startNorthOrientation + 180) % 360) > _endNorthOrientation)
            {
                //Rotation vers la gauche
                if ( (_startNorthOrientation - _endNorthOrientation) > 180 )
                {
                    _endNorthOrientation +=360;
                }
            } else {
                //Rotation vers la droite
                if ( (_endNorthOrientation - _startNorthOrientation) > 180 )
                {
                    _startNorthOrientation +=360;
                }
            }

            //Nouvelle animation
            _startTime = SystemClock.uptimeMillis();
            postDelayed(animationTask, DELAY);
        }
    }

    //~--- methods ------------------------------------------------------------


    /**
     * Initialisation de la vue
     */
    private void initView() {
        Resources r = this.getResources();

        // Paint pour l'arrière plan de la boussole
        _circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        _circlePaint.setStyle(Paint.Style.STROKE);
        //_circlePaint.setColor(r.getColor(R.color.compassCircle));

        // Paint pour les 2 aiguilles, Nord et Sud
        _northPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        _northPaint.setColor(r.getColor(R.color.northPointer));
       /* southPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        southPaint.setColor(r.getColor(R.color.southPointer));*/

        // Path pour dessiner les aiguilles
        _trianglePath = new Path();
    }

    // Permet de définir la taille de notre vue
    // /!\ par défaut un cadre de 100x100 si non redéfini

    /**
     * Permet de definir la taille de notre vue, cadre de 100x100 par defaut
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measuredWidth  = measure(widthMeasureSpec);
        int measuredHeight = measure(heightMeasureSpec);

        // Notre vue sera un carré, on garde donc le minimum
        int d = Math.min(measuredWidth, measuredHeight);

        setMeasuredDimension(d, d);
    }

    // Déterminer la taille de notre vue

    /**
     * Determiner la taille de notre vue
     * @param measureSpec
     * @return result
     */
    private int measure(int measureSpec) {
        int result   = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.UNSPECIFIED) {

            // Taille par défaut
            result = 200;
        } else {

            // On va prendre la taille de la vue parente
            result = specSize;
        }

        return result;
    }


    /**
     * Redessine la vue
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        int centerX = getMeasuredWidth() / 3;
        int centerY = getMeasuredHeight() / 3;

        // On détermine le diamètre du cercle (arrière plan de la boussole)
        int radius = Math.min(centerX, centerY);

        canvas.drawCircle(centerX, centerY, radius, _circlePaint);

        // On sauvegarde la position initiale du canvas
        canvas.save();

        // On tourne le canvas pour que le nord pointe vers le haut
        canvas.rotate(-_northOrientation, centerX, centerY);

        // on créer une forme triangulaire qui part du centre du cercle et
        // pointe vers le haut
        _trianglePath.reset();    // RAZ du path (une seule instance)
        _trianglePath.moveTo(centerX, 10);
        _trianglePath.lineTo(centerX - 10, centerY);
        _trianglePath.lineTo(centerX + 10, centerY);

        // On désigne l'aiguille Nord
        canvas.drawPath(_trianglePath, _northPaint);

        // On tourne notre vue de 180° pour désigner l'auguille Sud
       /* canvas.rotate(180, centerX, centerY);
        canvas.drawPath(_trianglePath, southPaint);*/

        // On restaure la position initiale (inutile, mais prévoyant)
        canvas.restore();
    }
}
