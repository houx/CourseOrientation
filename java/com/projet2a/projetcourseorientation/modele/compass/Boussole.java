/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.modele.compass;


import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Classe permettant d'utiliser la boussole.
 */

public class Boussole extends Service {


    private static final String TAG = "BroadcastService";
    public static final String BROADCAST_ACTION = "com.websmithing.broadcasttest.displayevent";
    private final Handler handler = new Handler();
    private Intent _intent;

    //La vue de notre boussole
    private CompassView compassView;

    //Le gestionnaire des capteurs
    private SensorManager _sensorManager;
    //Notre capteur de la boussole numérique
    private Sensor _sensor;

    //Notre listener sur le capteur de la boussole numérique
    private final SensorEventListener _sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            _intent.putExtra("rotation", String.valueOf(event.values[SensorManager.DATA_X]));
            sendBroadcast(_intent);
            // updateOrientation(event.values[SensorManager.DATA_X]);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    /**
     * Appele lorsque le service est instancie
     */

    @Override
    public void onCreate() {
        super.onCreate();

        _intent = new Intent(BROADCAST_ACTION);

        //setContentView(R.layout.boussole);
       /* LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.activity_course,null);
        compassView = (CompassView)layout.findViewById(R.id.compassView);*/
        //Récupération du gestionnaire de capteurs
        _sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        //Demander au gestionnaire de capteur de nous retourner les capteurs de type boussole
        List<Sensor> sensors = _sensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        //s’il y a plusieurs capteurs de ce type on garde uniquement le premier
        if (sensors.size() > 0) {
            _sensor = sensors.get(0);
        }
    }

    /**
     *
     * Appele par android juste apres le demarrage du service
     */
    /**
     * appele par android juste apres le demarrage du service
     * @param intent
     * @param flags
     * @param startId
     * @return service
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 1000);
        return Service.START_STICKY;
    }

    /**
     * Envoi les MaJ à l'UI
     */
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            _sensorManager.registerListener(_sensorListener, _sensor, SensorManager.SENSOR_DELAY_NORMAL);
            handler.postDelayed(this, 10000); // 10 seconds
        }
    };


    /**
     * Methode pour detruire la boussole.
     */
    @Override
    public void onDestroy() {

        _sensorManager.unregisterListener(_sensorListener);
        handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
    }


    /**
     * Methode a override qui retourne null.
     * @param intent
     * @return null
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
