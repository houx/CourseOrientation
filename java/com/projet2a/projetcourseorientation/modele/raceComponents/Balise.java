/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.modele.raceComponents;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Classe configurant les balises.
 */

public class Balise implements Parcelable{
    private int _id;
    private boolean _isChecked;
    private String _name;
    private double _latitude;
    private double _longitude;
    private String _type;
    private int _distancePreviousBalise;
    private String _unit;
    private double _xPos=0;
    private double _yPos=0;

    /**
     * Constructeur par defaut
     */
    public Balise(){
        _id = 0;
        _isChecked = false;
        _name = "start";
        _latitude = 0;
        _longitude = 0;
        _unit = "";

    }

    /**
     * Constructeur avec arguments
     * @param id
     * @param content
     * @param lat
     * @param lng
     * @param unit
     */
    public Balise(int id, String content, double lat, double lng, String unit)
    {
        _id = id;
        _isChecked = false;
        _name = content;
        _latitude = lat;
        _longitude = lng;
        _unit = unit;
    }

    /**
     * Constructeur avec d'autres arguments
     * @param id
     * @param content
     * @param lat
     * @param lng
     * @param xPos
     * @param yPos
     * @param unit
     */
    public Balise(int id, String content, double lat, double lng,double xPos,double yPos, String unit)
    {
        _id = id;
        _isChecked = false;
        _name = content;
        _latitude = lat;
        _longitude = lng;
        _unit = unit;
        _xPos = xPos;
        _yPos = yPos;
    }

    /**
     * Constructeur a partir d'un Parcel
     * @param in
     */
    protected Balise(Parcel in) {
        _id = in.readInt();
        _isChecked = in.readByte() != 0;
        _name = in.readString();
        _latitude = in.readDouble();
        _longitude = in.readDouble();
        _type = in.readString();
        _distancePreviousBalise = in.readInt();
        _unit = in.readString();
        _xPos = in.readDouble();
        _yPos = in.readDouble();
    }

    public static final Creator<Balise> CREATOR = new Creator<Balise>() {
        @Override
        public Balise createFromParcel(Parcel in) {
            return new Balise(in);
        }

        @Override
        public Balise[] newArray(int size) {
            return new Balise[size];
        }
    };

    /**
     * Setter du type
     * @param type
     */
    public void set_type(String type) { _type = type;}

    /**
     * Setter de la distance
     * @param distance
     */
    public void set_distance(int distance) { _distancePreviousBalise = distance;}

    /**
     * Getter de _isChecked
     * @return _isChecked
     */
    public boolean isChecked()
    {
        return _isChecked;
    }

    /**
     * Valide la balise
     */
    public void valideBalise()
    {
        _isChecked = true;
    }

    /**
     * Reset la balise
     */
    public void resetBalise()
    {
        _isChecked = false;
    }

    /**
     * Getter du nom de la balise
     * @return String _name
     */
    public String getName()
    {
        return _name;
    }

    /**
     * Getter de la position
     * @return xy
     */
    public double[] get_xyPos(){
        double[] xy = new double[2];
        xy[0] = _xPos;
        xy[1] = _yPos;
        return xy;
    }

    /**
     * Getter de la longitude/latitude
     * @return longLat
     */
    public double[] get_LongLat(){
        double[] longLat = new double[2];
        longLat[0] = _longitude;
        longLat[1] = _latitude;
        return longLat;
    }

    /**
     * Methode necessaire a l'override renvoyant 0
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * ECrit vers le Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeByte((byte) (_isChecked ? 1 : 0));
        dest.writeString(_name);
        dest.writeDouble(_latitude);
        dest.writeDouble(_longitude);
        dest.writeString(_type);
        dest.writeInt(_distancePreviousBalise);
        dest.writeString(_unit);
        dest.writeDouble(_xPos);
        dest.writeDouble(_yPos);
    }
}