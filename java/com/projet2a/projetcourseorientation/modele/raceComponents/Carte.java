/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.modele.raceComponents;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.String;
import java.util.ArrayList;

import com.projet2a.projetcourseorientation.MainActivity;


/**
 * Classe configurant la carte.
 */

public class Carte implements Parcelable{

    private static final String TAG = MainActivity.class.getSimpleName();
    private int _id;
    private String[] _baliseContent;
    private int _nbBalises;
    private int _length;
    private int _climb;
    private String _name;
    private ArrayList<Balise> _baliseArray;
    private int _nextBalise;
    private String _drawableName;


    /**
     * Constructeur de la carte depuis un nom
     * @param name
     */
    public Carte(String name) {
        _id = 0;
        _nbBalises = 1;
        _name = name;
        String[] _baliseContent = new String[1];
        _baliseContent[0] = "start";
        _length = 0;
        _climb = 0;
        _drawableName = "";
        _nextBalise = 0;
        _baliseArray = new ArrayList();
        _baliseArray.add(0, new Balise());
    }

    /**
     * Constructeur a partir d'un ensemble de balises en plus du nom
     * @param name
     * @param baliseArray
     */
    public Carte(String name, ArrayList<Balise> baliseArray) {
        _name = name;
        _baliseArray = baliseArray;
        _baliseContent = new String[_baliseArray.size()];
        _nextBalise = 0;

        for(int i = 0 ; i < _baliseArray.size() ; i++){
            _baliseContent[i] = _baliseArray.get(i).getName();
        }

    }

    /**
     * Constructeur a partir d'un Parcel
     * @param in
     */
    protected Carte(Parcel in) {
        _id = in.readInt();
        _baliseContent = in.createStringArray();
        _nbBalises = in.readInt();
        _length = in.readInt();
        _climb = in.readInt();
        _name = in.readString();
        _nextBalise = 0;
        _baliseArray = in.createTypedArrayList(Balise.CREATOR);
        _drawableName = in.readString();
    }

    public static final Creator<Carte> CREATOR = new Creator<Carte>() {
        @Override
        public Carte createFromParcel(Parcel in) {
            return new Carte(in);
        }

        @Override
        public Carte[] newArray(int size) {
            return new Carte[size];
        }
    };

    /**
     * Reset les balises de la carte
     */
    public void resetAllBalises() {
        _nextBalise = 0;
        for (int i = 0; i < _baliseArray.size(); i++) {
            _baliseArray.get(i).resetBalise();
        }
    }

    /**
     * Verifie si les balises sont bien validees
     * @return bool
     */
    public boolean balisesAllChecked()
    {
        for (int i = 0; i < _baliseArray.size(); i++)
        {
            if (!_baliseArray.get(i).isChecked())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Verifie si la carte contient une balise donnee
     * @param contents
     * @return bool
     */
    public boolean containsBalise(String contents) {
        for (Balise b : _baliseArray) {
            if (contents.equals(b.getName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Valide la balise
     * @param contents
     * @return bool
     */
    public boolean valideBalise(String contents) {
        Balise next = _baliseArray.get(_nextBalise);
        if (next.getName().equals(contents)) {
            if (! next.isChecked()) {
                next.valideBalise();
                _nextBalise++;
                return true;
            }
        }

        return false;
    }


    /**
     * Getter de l'id
     * @return id
     */
    public int getId()
    {
        return _id;
    }

    /**
     * Getter d'une balise d'un index donne
     * @param index
     * @return balise
     */
    public Balise getBalises(int index)
    {
        return _baliseArray.get(index);
    }


    /**
     * Getter du nombre de balises
     * @return int
     */
    public int getNbBalises()
    {
        return _baliseArray.size();
    }

    /**
     * Getter de la balise suivante
     * @return balise
     */
    public int getNextBalise() { return _nextBalise; }

    /**
     * Getter du nom de la carte
     * @return name
     */
    public String getName(){return _name;}

    /**
     * Getter du drawable_name
     * @return name
     */
    public String get_drawableName(){return _drawableName;}

    /**
     * Setter du drawable_name
     * @param name
     */
    public void set_drawableName(String name){_drawableName = name;}


    /**
     * Methode a override renvoyant 0
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Ecrit vers le Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeStringArray(_baliseContent);
        dest.writeInt(_nbBalises);
        dest.writeInt(_length);
        dest.writeInt(_climb);
        dest.writeString(_name);
        dest.writeTypedList(_baliseArray);
        dest.writeString(_drawableName);
    }


}