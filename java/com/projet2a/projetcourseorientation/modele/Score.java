/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 *
 */

package com.projet2a.projetcourseorientation.modele;

import android.app.Activity;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.projet2a.projetcourseorientation.MainActivity;
import com.projet2a.projetcourseorientation.R;

/**
 * Classe utilisee lors de la fin d'une course pour l'affichage des scores.
 */

public class Score implements Parcelable{
    private static final String TAG = MainActivity.class.getSimpleName();

    private long _time;
    private long [] _tabTime;
    private int _nbBalises;
    private Activity _activity;

    /**
     * Constructeur du score
     * @param nbBalises
     * @param activity
     */
    public Score(int nbBalises,Activity activity)
    {
        _nbBalises = nbBalises;
        _tabTime = new long[_nbBalises];
        _activity = activity;
    }

    /**
     * Constructeur du score a partir du Parcel.
     * @param in
     */
    protected Score(Parcel in) {
        _time = in.readLong();
        _tabTime = in.createLongArray();
        _nbBalises = in.readInt();
    }

    public static final Creator<Score> CREATOR = new Creator<Score>() {
        @Override
        public Score createFromParcel(Parcel in) {
            return new Score(in);
        }

        @Override
        public Score[] newArray(int size) {
            return new Score[size];
        }
    };

    /**
     * MaJ le score
     * @param index
     * @param time
     */
    public void updateScore(int index, long time)
    {
        _tabTime[index] = SystemClock.elapsedRealtime() - time;
    }

    /**
     * Affiche le temps total
     * @param textElement
     * @param str
     */
    public void showSimpleScore(TextView textElement, String str)
    {
        int hours  = Math.round(_time / 3600000);
        int minutes = Math.round((_time - hours * 3600000) / 60000);
        int seconds = Math.round((_time - hours * 3600000 - minutes * 60000) / 1000);
        textElement.setText(str + " " + hours + "h" + minutes + "min" + seconds + "s");
    }

    /**
     * Affiche le score detaille
     * @param tableScore
     */
    public void showDetailedScore(TableLayout tableScore)
    {
        int[] hours = new int[_tabTime.length];
        int[] minutes = new int[_tabTime.length];
        int[] seconds = new int[_tabTime.length];
        int i;

        TableRow row2;
        TextView tv2,tv3;


        for (i = 0; i<_tabTime.length; i++) {
            hours[i] = Math.round(_tabTime[i] / 3600000);
            minutes[i] = Math.round((_tabTime[i] - hours[i] * 3600000) / 60000);
            seconds[i] = Math.round((_tabTime[i] - hours[i] * 3600000 - minutes[i] * 60000) / 1000);

            row2 = new TableRow(_activity);
            tv2 = new TextView(_activity);
            tv2.setText(hours[i] + " h " + minutes[i] + "min" + seconds[i] + "s");
            tv2.setGravity(Gravity.CENTER);
            tv2.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1));
            Log.d(TAG, "TV2:"+ tv2.getText());
            tv3 = new TextView(_activity);
            if (i != 0) {
                int hoursB, minutesB, secondsB;
                hoursB = hours[i] - hours[i - 1];
                minutesB = minutes[i] - minutes[i - 1];
                secondsB = seconds[i] - seconds[i - 1];
                tv3.setText(hoursB + " h " + minutesB + "min" + secondsB + "s");
            } else {
                tv3.setText(hours[i] + " h " + minutes[i] + "min" + seconds[i] + "s");
            }
            tv3.setGravity(Gravity.CENTER);
            tv3.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1));
            Log.d(TAG, "TV3"+ tv3.getText());
            row2.addView(tv2);
            row2.addView(tv3);
            tableScore.addView(row2);


        }
    }

    /**
     * Gere la fin du temps
     * @param chrono
     */
    public void setEndTime(Chronometer chrono)
    {
        _time =  SystemClock.elapsedRealtime() - chrono.getBase();
    }

    /**
     * Methode a override renvoyant 0
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Ecrit dans le Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_time);
        dest.writeLongArray(_tabTime);
        dest.writeInt(_nbBalises);
    }

    /**
     * Setter de l'activite
     * @param activity
     */
    public void set_activity(Activity activity){
        _activity = activity;
    }

    /**
     * Getter du temps
     * @return time
     */
    public long getTemps()
    {
        return _time;
    }

    /**
     * Getter de tous les sous-temps
     * @return tabtime
     */
    public long[] getTabTemps(){return _tabTime;}
}
