/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.modele;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.Toast;

import com.projet2a.projetcourseorientation.modele.compass.CompassView;
import com.projet2a.projetcourseorientation.MainActivity;
import com.projet2a.projetcourseorientation.R;
import com.projet2a.projetcourseorientation.modele.raceComponents.Carte;

import java.io.UnsupportedEncodingException;


/**
 * Classe utilisee lors du deroulement d'une course, elle permettra d'initialiser ou de re-initialiser les etats des balises et le chronometre.
 */
public class Course implements Parcelable{

    private static final String TAG = MainActivity.class.getSimpleName();
    private Carte _carte;
    private int _lastCheckedBalise;
    private Chronometer _chrono;
    private Score _score;
    private Activity _activity;
    private long _time;
    private String _mode;

    /**
     * Constructeur d'une course
     * @param carte
     * @param mode
     * @param activity
     */
    public Course(Carte carte, String mode, Activity activity)
    {
        _carte = carte;
        _mode = mode;
        _score = new Score(_carte.getNbBalises(),activity);
        _activity = activity;
        _lastCheckedBalise = 0;
        initChrono();

        if(_mode.equals("famille")){
            _chrono.setVisibility(View.INVISIBLE);
            CompassView compass = (CompassView) _activity.findViewById(R.id.compassView);
            compass.setVisibility(View.INVISIBLE);
        }

    }

    /**
     * Constructeur d'une course a partir d'un Parcel
     * @param in
     */
    protected Course(Parcel in) {
        _carte = in.readParcelable(Carte.class.getClassLoader());
        _lastCheckedBalise = in.readInt();
        _score = in.readParcelable(Score.class.getClassLoader());
        _time = in.readLong();
    }

    public static final Creator<Course> CREATOR = new Creator<Course>() {
        @Override
        public Course createFromParcel(Parcel in) {
            return new Course(in);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };


    /* MANAGEMENT COURSE*/


    /**
     * Appele lorsque l'on scanne une balise une fois la course commencee
     * @param contents
     * @return bool
     */
    public boolean baliseValidation(String contents) {

        String text;
        boolean baliseScanned = false;

        if (_carte.containsBalise(contents)) {

            if (_carte.valideBalise(contents)) {
                text = _activity.getString(R.string.balise, contents);
                baliseScanned = true;
            } else {
                text = _activity.getString(R.string.wrongBalise, _carte.getNextBalise() + "");
            }

        } else {
            text = _activity.getString(R.string.baliseUndetermined);
        }

        Toast toast = Toast.makeText(_activity, text, Toast.LENGTH_LONG);
        toast.show();


        /*if ((_lastCheckedBalise == 0) && contents.equals(_carte.getBalises(_lastCheckedBalise).getName())) {
            chaine = _activity.getString(R.string.balise, contents);
            _carte.getBalises(_lastCheckedBalise).valideBalise();
            baliseScanned = true;
        } else {

            if (((_lastCheckedBalise + 1 < _carte.getNbBalises())) && (Arrays.asList(_carte.getBaliseContentList()).contains(contents))) {

                chaine =  _activity.getString(R.string.balise, contents);

                if (contents.equals(_carte.getBalises(_lastCheckedBalise + 1).getName())) {
                    _lastCheckedBalise++;
                    _carte.getBalises(_lastCheckedBalise).valideBalise();
                    baliseScanned = true;
                }
            }
        }
*/

        return baliseScanned;
    }

    /**
     * Renvoi le statut actuel de la course
     * @return out
     */
    public String getCourseState(){
        String out = "";
        for (int i = 1; i < _carte.getNbBalises() - 1 ; i++){
            out += _activity.getString(R.string.namebalise) + i + " : ";
            if((_carte.getBalises(i)).isChecked())
                out += _activity.getString(R.string.valide) +"\n";
            else
                out+= _activity.getString(R.string.nonValide) + "\n";
        }
        return out;
    }

    /**
     * Verifie si toutes les balises sont validees
     * @return bool
     */
    public boolean isSuccess()
    {
        return _carte.balisesAllChecked();
    }

    /**
     * Getter de la carte
     * @return carte
     */
    public Carte getCarte()
    {
        return _carte;
    }

    /**
     * Getter du score
     * @return score
     */
    public Score getScore()
    {
        return _score;
    }

    /**
     * Reset la course
     */
    public void resetRace() {
        _carte.resetAllBalises();
    }

    /* MANAGEMENT CHRONO*/

    /**
     * Initialise le chrono
     */
    public void initChrono() {
        _chrono = (Chronometer) _activity.findViewById(R.id.chronometer);
    }

    /**
     * Demarre le chrono
     */
    public void startChrono() {
        _chrono.setBase(SystemClock.elapsedRealtime());
        _chrono.start();
    }

    /**
     * Stop le chrono
     */
    public void stopChrono() {
        _score.setEndTime(_chrono);
        _chrono.stop();
    }

    /**
     * MaJ le chrono
     */
    public void updateTime()
    {
        _score.updateScore(_carte.getNextBalise() - 1, _chrono.getBase());
    }

    /* USAGE POUR NFC*/

    /**
     * Recupere le NFC
     * @param intent
     * @return message
     */
    public String getNFCContent(Intent intent) {
        String action = intent.getAction();
        String message = "resoudreIntent";
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] messages;
            if (rawMsgs != null) {
                messages = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    messages[i] = (NdefMessage) rawMsgs[i];
                    NdefRecord record = messages[i].getRecords()[i];
                    message = getTextData(record.getPayload());
                }
            }
        }
        return message;
    }

    /**
     *
     * @param payload
     * @return string
     */
    String getTextData(byte[] payload) {
        String texteCode;
        if(payload == null)
            return null;
        try {
            if ((payload[0] & 0200) == 0) {
                texteCode = "UTF-8";
            } else {
                texteCode = "UTF-16";
            }
            int langageCodeTaille = payload[0] & 0077;
            return new String(payload, langageCodeTaille + 1, payload.length -langageCodeTaille - 1, texteCode);
        } catch(UnsupportedEncodingException e)
        {
            Log.e("NfcReaderActivity", e.getMessage());
            return null;
        }
    }

    /**
     * Getter du nom de la carte
     * @return name
     */
    public String getName(){
        return _carte.getName();
    }

    /**
     * Methode a override renvoyant 0
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Ecrit dans le Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(_carte, flags);
        dest.writeInt(_lastCheckedBalise);
        dest.writeParcelable(_score, flags);
        dest.writeLong(_time);
    }

}
