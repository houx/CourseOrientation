/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */
package com.projet2a.projetcourseorientation.secondaryActivities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.projet2a.projetcourseorientation.modele.compass.CompassView;
import com.projet2a.projetcourseorientation.MainActivity;
import com.projet2a.projetcourseorientation.R;
import com.projet2a.projetcourseorientation.utils.map.MapInfo;
import com.projet2a.projetcourseorientation.modele.compass.Boussole;
import com.projet2a.projetcourseorientation.modele.Course;
import com.projet2a.projetcourseorientation.modele.raceComponents.Carte;
import com.projet2a.projetcourseorientation.utils.map.GPSTracker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * * Classe utilisee pour le deroulemet d'une course dans n'importe quel mode.
 */

public class CourseActivity extends AppCompatActivity {
    private static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private static final String TAG = MainActivity.class.getSimpleName();
    private final static int CHOOSE_QR_CODE = 0;
    private static final int DIALOG_ALERT = 10;
    private boolean _start = false;
    private Course _course = null;
    private ScaleGestureDetector _SGD;
    private ImageView _iv;
    private Matrix _matrix = new Matrix();
    private float _scale = 1f;
    private int _currentX;
    private int _currentY;
    private ImageView map;
    private MapInfo _mapinfo = null;
    private NfcAdapter _nfcAdapter;
    private Carte _carte;
    private Intent _intent;
    private String _mode;
    private String _affichage;
    private SharedPreferences _spScore;
    private SharedPreferences _spTaille;
    private String _pseudo="Pierrick";
    private boolean _ranked= true;
    private File _gpxFile;
    private boolean _isRescaling = false;
    private boolean _isMoving = false;
    private View _stockView;
    private GPSTracker _gps;
    private boolean _validatedScores = false;


    /**
     * Envoie les scores au serveur
     */
    private void sendScoresToServer() {


        final AQuery aq = new AQuery(this);



        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if (json != null) {
                    Toast.makeText(aq.getContext(), status.getCode() + ":" + json.toString(), Toast.LENGTH_LONG).show();
                    System.out.println(json.toString());

                } else {
                    Toast.makeText(aq.getContext(), "Status:" + status.getCode(), Toast.LENGTH_LONG).show();
                }

            }
        };

        //cb.header("Content-Type", "multipart/form-data; charset=utf-8");

        Map<String, Object> map = recupInfoGlobale();
        map.put("trace_gps", _gpxFile);
        //System.err.println(new JSONObject(map));
        //Toast toast = Toast.makeText(this, new JSONObject(map).toString(), Toast.LENGTH_LONG);
        //toast.show();
        aq.ajax("http://appli.yann-p.fr:8080/course", map, JSONObject.class, cb);


    }


    /**
     * Fenetre qui rentre le pseudo de l'utilisateur
     */
    private void showSendToServerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Envoyer les scores au serveur");

        final EditText input = new EditText(this);
        input.setHint("Entrez votre nom et prénom");
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                _pseudo = input.getText().toString();
                _validatedScores = true;
                sendScoresToServer();

            }
        });



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if(!_validatedScores) {
                        showSendToServerDialog();
                    }
                }
            });

        }


        builder.show();
    }

    /**
     * Recupere les infos de la course de l'utilisateur pour les envoyer au serveur
     * @return infos
     */
    public HashMap<String,Object> recupInfoGlobale()
    {
        HashMap<String,Object> map = new HashMap<>();
        map.put("pa_id","4");
        map.put("mode",_mode);
        map.put("pseudo",_pseudo);
        map.put("temps",String.valueOf(_course.getScore().getTemps()));
        map.put("est_classe", _ranked ? "O" : "N");

        long[] tabTemps = _course.getScore().getTabTemps();

        long [] tabTempsParse = new long[tabTemps.length-1];

        for(int i = 1; i < tabTemps.length; i++) {
            tabTempsParse[i-1] = tabTemps[i];
        }

        ArrayList<HashMap<String, String>> sousTemps = new ArrayList<>();

        int n = tabTemps.length;
        for (int i=0;i<n-1;i++)
        {
            HashMap<String, String> sousTempsEntry = new HashMap<String,String>();
            sousTempsEntry.put("balise_1",_course.getCarte().getBalises(i).getName());
            sousTempsEntry.put("balise_2",_course.getCarte().getBalises(i+1).getName());
            sousTempsEntry.put("temps",String.valueOf(tabTempsParse[i]));
            sousTemps.add(sousTempsEntry);
        }

//        map.put("sous_temps", Arrays.toString(tabTempsParse));
        System.err.println("Sous-temps : " + new JSONArray(sousTemps));
        map.put("sous_temps", new JSONArray(sousTemps));
        map.put("est_classe", _ranked ? "O" : "N");

        return map;
    }


    /**
     * Methode appele a la creation.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);


        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
        {
            _affichage = bundle.getString("affichageAppli");
            if(_affichage.equals("portrait")) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            if(_affichage.equals("paysage")) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }



        // DONNEES CARTE+COURSE
        _carte = getIntent().getExtras().getParcelable("CARTE");
        _mode = getIntent().getExtras().getString("modeAppli");
        Log.d("mode : ", _mode);
        _course = new Course(_carte,_mode,this);

        _iv = (ImageView)findViewById(R.id.expanded_image);
        _SGD = new ScaleGestureDetector(this,new ScaleListener());
        map = (ImageView)findViewById(R.id.expanded_image);

        // AFFICHAGE BARRE DE MENU
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(_carte.getName());

        //NFC
        NfcManager _manager = (NfcManager) this.getSystemService(Context.NFC_SERVICE);
        _nfcAdapter = _manager.getDefaultAdapter();

        //INITIALISATION DE L'IMAGE DE LA CARTE
        File imgFile = new  File(getFilesDir().toString()+ '/' + "map" + '/' +_carte.get_drawableName());

        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ImageView myImage = (ImageView) findViewById(R.id.expanded_image);
            myImage.setImageBitmap(myBitmap);

            Log.i(TAG, "ok Changement de map");
        }
        else
            Log.i(TAG, "Erreur chargement map"+_carte.get_drawableName());
        //INITIALISATION POUR AFFICHAGE MAP
        if (_mapinfo == null) {
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            int heightPixels = metrics.heightPixels;
            int widthPixels = metrics.widthPixels;
            _mapinfo = new MapInfo((ImageView) findViewById(R.id.expanded_image),widthPixels,heightPixels);


            //if (_mode.equals("famille")) {
            initModeBalade();
                //affichageChoixBallade();
            //}//

        }
        _intent = new Intent(this, Boussole.class);
        _spScore = getSharedPreferences("PrefScores", 0);
        _spTaille = getSharedPreferences("PrefTaille", 0);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        _stockView = inflater.inflate(R.layout.stock_score,null);
    }

    /* METHODE POUR BOUSSOLE (VOIR ONRESUME POUR SON ACTIVATION) */
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUI(intent);
        }
    };

    /**
     * MaJ de l'UI
     * @param intent
     */
    private void updateUI(Intent intent) {
        float rotation = Float.parseFloat(intent.getStringExtra("rotation"));
        CompassView compass = (CompassView) findViewById(R.id.compassView);
        compass.setNorthOrientation(rotation);

    }

    /**
     * Affiche les scores
     */
    private void score(){
        Intent scoreDisplayActivity = new Intent(this, ScoresDisplayerActivity.class);
        setResult(RESULT_OK, scoreDisplayActivity);
        scoreDisplayActivity.putExtra("COURSE", _course);
        scoreDisplayActivity.putExtra("modeAppli",getIntent().getExtras().getString("modeAppli"));
        scoreDisplayActivity.putExtra("BOOLEAN_SIMPLE_SCORE", true);
        startActivity(scoreDisplayActivity);
    }

    /* MANAGEMENT DE LA COURSE */

    /**
     * Met fin à la course
     */
    private void endRace(){
        Log.d("fin course", "fin course");
        showSendToServerDialog();
        _course.stopChrono();
        _start = false;
        score();


        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), "trace.gpx");

        String gpxTrace = _gps.endRace();
        try {
            FileOutputStream f = new FileOutputStream(file);
            f.write(gpxTrace.getBytes());

            f.close();
            _gpxFile = file;
        } catch (IOException e) {
            e.printStackTrace();
        }

        
        _course.resetRace();
        Button qrcode = (Button) findViewById(R.id.scanner);
        qrcode.setText(R.string.startRace);
    }

    /**
     *
     * Demarre la course
     */
    private void startCourse() {
        _validatedScores = false;
        _course.startChrono();
        Button qrcode = (Button) findViewById(R.id.scanner);
        qrcode.setText(R.string.ScannerLaBaliseSuivante);
    }

    /**
     * Verifie si la balise est la balise start
     * @param str
     * @return bool
     */
    private boolean isStart(String str) {
        String firstBalise = _course.getCarte().getBalises(0).getName();
        if (str.equals(firstBalise))
        {
            _start = true;
            return true;
        }
        return false;
    }

    /**
     * Verifie si c'est la balise de fin
     * @param str
     * @return bool
     */
    private boolean isEnd(String str) {
        String lastBalise = _course.getCarte().getBalises(_course.getCarte().getNbBalises()-1).getName();
        return str.equals(lastBalise);
    }


    /**
     * Utilisation du scan QRCODE
     * @param act
     * @param title
     * @param message
     * @param buttonYes
     * @param buttonNo
     * @return alertdialog
     */
    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    /**
     * Permet de demarrer le scan
     */
    private void startScanMode() {
        Intent intent = new Intent(ACTION_SCAN);
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(intent, CHOOSE_QR_CODE);
    }

    /**
     * Permet de scanner, propose de telecharger un scanner si l'utilisateur n'en a pas.
     * @param v
     */
    public void scanQR(View v) {
        if (_start) {
            try {
                startScanMode();
            } catch (ActivityNotFoundException anfe) {
                showDialog(this, getString(R.string.noScannerFound), getString(R.string.askDownloadScanner), getString(R.string.yes), getString(R.string.no)).show();
            }
        }
        else {
            try {
                startScanMode();
                Toast toast = Toast.makeText(this, getString(R.string.canReadQRCodes), Toast.LENGTH_LONG);
                toast.show();
            } catch (ActivityNotFoundException anfe) {
                showDialog(this, getString(R.string.noScannerFound), getString(R.string.askDownloadScanner), getString(R.string.yes), getString(R.string.no)).show();
            }
        }
    }

    /**
     * Resultat du scan
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CHOOSE_QR_CODE) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                if (isStart(contents)){
                    startCourse();
                }
                if (_start) {
                    if (_course.baliseValidation(contents)){
                        _course.updateTime();
                    }
                }
                if (isEnd(contents))
                {
                    endRace();
                }
            }
        }
    }

    /**
     * Listener de l'echelle pour le zoom
     */
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if (_mapinfo == null) {
                return true;
            }

            _isRescaling = true;

            _scale *= detector.getScaleFactor();
            _scale = Math.max(1f, Math.min(_scale, 5.0f));


            map.setScaleX(_scale);

            map.setScaleY(_scale);


            return true;
        }


        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            _isRescaling = false;
        }
    }


    /**
     * Permet de deplacer la carte
     * @param event
     * @return bool
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (_mapinfo == null) {
            return true;
        }

        _SGD.onTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                if (_isRescaling)
                    break;

                _currentX = (int) event.getRawX();
                _currentY = (int) event.getRawY();

                break;
            }
            case MotionEvent.ACTION_MOVE: {

                if (_isRescaling)
                    break;

                int x2 = (int) event.getRawX();
                int y2 = (int) event.getRawY();


                int dx =(int)( (x2 - _currentX)/_scale);
                int dy = (int)( (y2 - _currentY)/_scale);
                if (Math.abs(dx) + Math.abs(dy) > 200)
                    return false;


                map.scrollBy(-dx, -dy);


                _currentX = x2;
                _currentY = y2;


                break;
            }
            case MotionEvent.ACTION_UP: {
                _isMoving = false;
                break;
            }
        }
        return true;
    }

    /**
     * Valide la balise
     * @param v
     */
    public void baliseValide(View v){
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(this);
        downloadDialog.setTitle(R.string.baliseValide);
        String out =_course.getCourseState();

        downloadDialog.setMessage(out);
        downloadDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        downloadDialog.show();
    }

    /* NFC (PRINCIPALEMENT POUR QUE L'APPLICATION RESTE AU PREMIER PLAN*/

    /**
     * Utilisation du NFC
     */
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (_nfcAdapter != null && _nfcAdapter.isEnabled()) {
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            _nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
        registerReceiver(broadcastReceiver, new IntentFilter(Boussole.BROADCAST_ACTION));
        startService(_intent);
    }

    /**
     * Demarrage de la course par le NFC
     * @param intent
     */
    public void onNewIntent(Intent intent) {
        String contents = _course.getNFCContent(intent);
        if (isStart(contents)){
            startCourse();
        }
        if (_start) {
            if (_course.baliseValidation(contents)){
                _course.updateTime();
            }
        }
        else {
            Toast toast = Toast.makeText(this, R.string.courseNonDebutee, Toast.LENGTH_LONG);
            toast.show();
        }
        if (isEnd(contents))
        {
            endRace();
        }
    }


    /* SCORES */

    /**
     * Recupere les scores
     */
    private void stockScore() {


        TextView textElement = (TextView) _stockView.findViewById((R.id.textScore));

        textElement.setText(R.string.score);

        String name = _course.getName();
        textElement.append(name + "\n");
        List<Long> list = new ArrayList<Long>();
        int size = _spScore.getInt(name + "_size", 0);
        for (int i = 1; i <= size; i++) {
            list.add(_spScore.getLong(name + i, 0));
        }

        if (list.isEmpty() || list == null) {
            //TODO Régler bug avec activité scoresdisplayerActivity, sans doute à cause du _activity.get....
            //textElement.append(_activity.getString(R.string.emptyScore));
            textElement.append("Pas de score");
        }

        for (int i = 0; i < list.size(); i++) {
            textElement.append("Course n°" + i + "  :  ");
            long temps = list.get(i);
            int hours = Math.round(temps / 3600000);
            int minutes = Math.round((temps - hours * 3600000) / 60000);
            int seconds = Math.round((temps - hours * 3600000 - minutes * 60000) / 1000);
            textElement.append(hours + "h" + minutes + "min" + seconds + "s \n");
        }
    }

    /**
     * Reset les scores et donc les temps
     */
    public void clearScore(){
        SharedPreferences.Editor edit= _spScore.edit();
        edit.clear();
        edit.commit();

        SharedPreferences.Editor edit2= _spTaille.edit();
        edit2.clear();
        edit2.commit();
        Toast toast = Toast.makeText(this,getString(R.string.clearScoreSucess), Toast.LENGTH_LONG);
        toast.show();

    }

    /**
     * Methode creant un dialoge (deprecated)
     * @param id
     * @return Dialog
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ALERT:
                // Create out AlterDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setView(_stockView);
                builder.setCancelable(true);
                builder.setPositiveButton("OK", new OkOnClickListener());
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            Toast.makeText(getApplicationContext(), "Activity will continue",
                    Toast.LENGTH_LONG).show();
        }
    }



    /**
     * Methode a override pour creer un menu option
     * @param menu
     * @return bool
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.carte_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Definition des options
     * @param item
     * @return bool
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()){
            case R.id.affichage:

                Toast.makeText(getBaseContext(), R.string.changeDisplayMode, Toast.LENGTH_SHORT).show();
                if(_affichage.equals("paysage")) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    _affichage = "portrait";
                }
                else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    _affichage = "paysage";
                }
                break;

            case R.id.abondonner:
                setResult(1);
                finish();
                break;
            case R.id.stockScore:
                stockScore();
                showDialog(DIALOG_ALERT);
                break;
            case R.id.clearScore:
                clearScore();
                break;
        }
        return true;

    }


    /**
     * Initialisation du mode ballade
     */
    private void initModeBalade() {
        Log.i("init gps", "JINITE LE MODE GPS");

        _gps = new GPSTracker(this);
        _gps.setObserver(this);


    }


}

