/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.secondaryActivities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projet2a.projetcourseorientation.MainActivity;
import com.projet2a.projetcourseorientation.R;
import com.projet2a.projetcourseorientation.modele.raceComponents.Carte;
import com.projet2a.projetcourseorientation.utils.CreateMapFromXML;
import com.projet2a.projetcourseorientation.utils.DownloadFileFromURL;
import com.projet2a.projetcourseorientation.utils.ObserverDownloadMenu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Classe utilisee pour choisir sa carte.
 */


public class ChooseMapActivity extends AppCompatActivity implements ObserverDownloadMenu {
    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    public static final int CHOOSE_QR_CODE = 0;

    private String _tag ="Test";
    private ArrayList<Carte>[] _cartes;
    private int _num_current_course =-1;
    final ArrayList<Integer> _nbMaps = new ArrayList<Integer>();
    private int _indexCarte=0;
    private String _filename=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_map);
        displayMap();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }


    /**
     * Valide le choix de la carte et lance la prochaine activite
     * @param v
     */
    public void validerChoix(View v){
        if(_num_current_course == -1){
            Intent result = new Intent();
            setResult(RESULT_CANCELED, result);
            finish();
        }else{
            if (validerChoix(true)) {
                startActivityCourse();
            }
        }
    }

    /**
     * Valide le choix
     * @param displayDialog
     * @return bool
     */
    public boolean validerChoix(boolean displayDialog){
        Toast toast;
        if (_num_current_course != -1)
        {
            _cartes[_indexCarte].get(_num_current_course).set_drawableName(_filename);
            String nameCourse = getFilesDir().toString() + '/' + "map" + '/' + _cartes[_indexCarte].get(_num_current_course).get_drawableName();
            File imgFile = new  File(nameCourse);
            if(imgFile.exists()){
                toast = Toast.makeText(this, getString(R.string.courseChoiceValide), Toast.LENGTH_LONG);
                toast.show();
            }
            else {
                if (displayDialog) {
                    showDialog(this, getString(R.string.problemeTelechargement), getString(R.string.messageProblemeTelechargement), getString(R.string.yes), getString(R.string.no));
                }
                return false;
            }
        }
        else {
            toast = Toast.makeText(this, getString(R.string.courseChoiceNoValide) + _num_current_course, Toast.LENGTH_LONG);
            toast.show();
            return false;
        }
        return true;
    }


    /**
     * Methode qui gère l'appui du bouton retour/
     */
    public void onBackPressed() {
        if(_num_current_course != -1 && (validerChoix(false))){
            startActivityCourse();
        }
        else{
            Intent result = new Intent();
            setResult(RESULT_CANCELED, result);
            finish();
        }
        super.onBackPressed();
    }

    /**
     * Demarre l'activite
     */
    private void startActivityCourse(){
        Intent intent = new Intent(getBaseContext(), CourseActivity.class);
        setResult(RESULT_OK, intent);
        int indexCarte = _nbMaps.get(_num_current_course);
        Carte carte = _cartes[indexCarte].get(_num_current_course);
        intent.putExtra("affichageAppli",getIntent().getExtras().getString("affichageAppli"));
        intent.putExtra("modeAppli",getIntent().getExtras().getString("modeAppli"));
        intent.putExtra("CARTE", carte);
        startActivityForResult(intent, 1000);
    }


    /**
     * Telechargement simple
     * @param v
     */
    public void download(View v){
        String _url2 = "http://www.ecole.ensicaen.fr/~ypellegrini/CourseOrientation/infoCourseEnsi.xml";
        String filename2 = "infoCourse.xml";
        DownloadFileFromURL download = new DownloadFileFromURL(true,getFilesDir().toString());
        download.addObserver(this);
        download.execute(_url2, "", filename2);

    }


    /**
     * Supprime les telechargement
     * @param v
     */
    public void supprimer(View v){

        File[] test = getFilesDir().listFiles();
        File[] test2;

        for (int i = 0 ; i < test.length ; i++){
            test2 = test[i].listFiles();
            if (test2 != null) {
                for (int j = 0; j < test2.length; j++) {
                    test2[j].delete();
                }
            }
            test[i].delete();
        }
        displayMap();
    }

    /* AFFICHAGE DE LA LISTE DE CARTES*/

    /**
     * Affichage de la liste des cartes
     */
    private class MessengerDisplayMap{

        public List<String> displayArray;
        public ArrayList<String> lienPositionHtml;
        public ArrayList<String> lienPositionName;
        public MessengerDisplayMap(){
            displayArray = new ArrayList<String>();
            lienPositionHtml = new ArrayList<String>();
            lienPositionName = new ArrayList<String>();
        }
    }


    /**
     * Affiche la carte
     */
    public void displayMap(){

        Log.w("Dnum_current_course",Integer.toString(_num_current_course));

        final View choiceCourseConteneur = findViewById(R.id.choiceCourseConteneur);
        final View dlFirst = findViewById(R.id.dlFirst);

        if (!isDiplayPossible()) {
            dlFirst.setVisibility(View.VISIBLE);
            choiceCourseConteneur.setVisibility(View.INVISIBLE);
            return;
        }
        dlFirst.setVisibility(View.INVISIBLE);
        choiceCourseConteneur.setVisibility(View.VISIBLE);

        final MessengerDisplayMap messengerDisplayMap = createMultipleArray();

        ListView choiceCourse = (ListView) findViewById(R.id.choiceCourse);


        choiceCourse.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, messengerDisplayMap.displayArray) {
            @Override
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);

                textView.setTextColor(Color.parseColor("#76B777"));

                return textView;
            }
        });

        final DownloadFileFromURL downloadFile = new DownloadFileFromURL(getFilesDir().toString());
        downloadFile.addObserver(this);

        choiceCourse.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView,
                                    View view,
                                    int position,
                                    long id) {

                String noDDl = getString(R.string.telechargementImpossible);
                String valueOfClick = messengerDisplayMap.lienPositionHtml.get(position);

                if (valueOfClick == noDDl) {
                    return;
                }

                String[] list = valueOfClick.split("/");
                _filename = list[list.length - 1];
                TextView download = (TextView) findViewById(R.id.currentCourse);

                _indexCarte = _nbMaps.get(position);
                _num_current_course = position;
                if (isFileExist(messengerDisplayMap.lienPositionHtml.get(position), "map")) {
                    download.setText(getString(R.string.currentCourse) + " : " + messengerDisplayMap.lienPositionName.get(position));
                    return;
                }
                downloadFile.execute(valueOfClick, "map", _filename);


                Log.i(_tag, "lien html=" + messengerDisplayMap.lienPositionHtml.get(position));
                download.setText(getString(R.string.currentCourse) + " : " + messengerDisplayMap.lienPositionName.get(position));
            }
        });
        Log.w("Fnum_current_course", Integer.toString(_num_current_course));
    }

    private MessengerDisplayMap createMultipleArray(){

        File[] test = new File(getFilesDir().toString() +'/'+"infoMap").listFiles();

        for(int i = 0; i<test.length; i++)
        {
            Log.i(TAG, "name file:" + test[i].getName());
        }

        int numberOfXmlFile=test.length;
        Log.i(TAG, "Number XML File= " + String.valueOf(numberOfXmlFile));
        CreateMapFromXML[] mapXml = new CreateMapFromXML[numberOfXmlFile];
        _cartes = new ArrayList[numberOfXmlFile];
        MessengerDisplayMap messengerDisplayMap = new MessengerDisplayMap();
        ArrayList<String> xmlFileNameFromRootArray = getFileNameFromRootFinishingBy(".xml");

        int nombreDeCarteTraite = 0;
        for (int i = 0; i < numberOfXmlFile; i++) {
            mapXml[i] = new CreateMapFromXML(test[i].toString());
            _cartes[i] = mapXml[i].createArrayMap();

            for (int k=0 ; k < xmlFileNameFromRootArray.size() ; k++) {
                String xmlFileNameFromRoot = getFilesDir().toString() + '/' + xmlFileNameFromRootArray.get(k);
                CreateMapFromXML map = new CreateMapFromXML(xmlFileNameFromRoot);
                Map<String, String> mapHtml = map.createMapLienDownload();
                for (int j = 0; j < _cartes[i].size(); j++) {
                    String name = _cartes[i].get(j).getName();
                    if( messengerDisplayMap.lienPositionName.contains(name)){
                        if (mapHtml.containsKey(name)) {
                            updateDisplayArray(messengerDisplayMap, nombreDeCarteTraite, mapHtml, j, name, true);
                        }
                    }else {
                        messengerDisplayMap.lienPositionName.add(j + nombreDeCarteTraite, name);
                        _nbMaps.add(j + nombreDeCarteTraite, i);
                        if (mapHtml.containsKey(name)) {
                            updateDisplayArray(messengerDisplayMap, nombreDeCarteTraite, mapHtml, j, name, false);
                        } else {
                            messengerDisplayMap.displayArray.add(j + nombreDeCarteTraite, name + " : " + getString(R.string.telechargementImpossible));
                            messengerDisplayMap.lienPositionHtml.add(j + nombreDeCarteTraite, getString(R.string.telechargementImpossible));
                        }
                    }
                }
            }
            nombreDeCarteTraite += _cartes[i].size();
        }

        return messengerDisplayMap;
    }

    /**
     * Maj du tableau
     * @param messengerDisplayMap
     * @param nombreDeCarteTraite
     * @param mapHtml
     * @param j
     * @param name
     * @param override
     */
    private void updateDisplayArray(MessengerDisplayMap messengerDisplayMap, int nombreDeCarteTraite, Map<String, String> mapHtml, int j, String name, boolean override) {
        if(override) {
            messengerDisplayMap.lienPositionHtml.clear();
            messengerDisplayMap.displayArray.clear();
        }

        messengerDisplayMap.lienPositionHtml.add(j + nombreDeCarteTraite, mapHtml.get(name));
        if (isFileExist(mapHtml.get(name), "map")) {
            messengerDisplayMap.displayArray.add(j + nombreDeCarteTraite, name + " : " + getString(R.string.dejaTelecharge));//+ mapHtml.get(name);
        } else {
            messengerDisplayMap.displayArray.add(j + nombreDeCarteTraite, name + " : " + getString(R.string.telechargement));//+ mapHtml.get(name);
        }
    }

    /**
     * Retourne si l'affichage est possible
     * @return bool
     */
    private boolean isDiplayPossible() {
        if( !isFileExist("infoMap",null)){
            return false;
        }
        return true;
    }

    /**
     * Affiche le dialogue
     * @param act
     * @param title
     * @param message
     * @param buttonYes
     * @param buttonNo
     */
    private void showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent result = new Intent();
                setResult(RESULT_CANCELED, result);
                finish();
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        downloadDialog.show();
    }

    /**
     * Retourne l'existence d'un fichier
     * @param url
     * @param nameDir
     * @return
     */
    private boolean isFileExist(String url,String nameDir){
        String[] list = url.split("/");
        String nomDuFichier = list[list.length-1];
        File dir;

        if (nameDir != null){
            dir = new File(getFilesDir().toString()
                    + '/' + nameDir+'/' + nomDuFichier);
        }
        else{
            dir = new File(getFilesDir().toString()
                    + '/' + nomDuFichier);
        }

        if (dir.exists()) {
            return true;
        }
        return false;
    }

    /**
     * Montre le dialogue
     * @param act
     * @param title
     * @param message
     * @param buttonYes
     * @param buttonNo
     * @return
     */
    private static AlertDialog showDialog2(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    /**
     * Telecharge via un qrcode
     * @param v
     */
    public void downloadQRCode(View v) {
        try {
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, CHOOSE_QR_CODE);
        } catch (ActivityNotFoundException anfe) {
            showDialog2(this, getString(R.string.noScannerFound), getString(R.string.askDownloadScanner), getString(R.string.yes), getString(R.string.no)).show();
        }
    }

    /**
     * Affiche un resultat
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CHOOSE_QR_CODE) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                Log.w("contenu de la balise : ", contents);
                analyseEtInterpretationDeLURL(contents);
            }
        }

        //la requete vient de CourseActivity
        if(requestCode==1000){
            // si le code de retour est égal à 1 on stoppe l'activité 1
            if(resultCode==1){
                finish();
            }
        }
    }

    /**
     * Renvoie le nom du fichier depusi l'url
     * @param url
     * @return filename
     */
    public String filenamenFromUrl(String url){
        String[] list = url.split("/");
        String nomDuFichier = list[list.length-1];
        return nomDuFichier;
    }

    /**
     * Analyse et interprete l'url
     * @param url
     */
    public void analyseEtInterpretationDeLURL(String url){
        String filename = filenamenFromUrl(url);

        if (filename.endsWith("tmp.xml")){
            Log.w("extention : ", "xml et fichier tmp");
            download(url, filename, true);
        }
        else if (filename.endsWith(".xml")){
            Log.w("extention : ", "xml");
            download(url, filename, false);
        }
        else{
            Log.w("extention : ", "pas xml");
        }
    }

    /**
     * Telecharge
     * @param url
     * @param filename
     * @param ddlForMap
     */
    private void download(String url, String filename, boolean ddlForMap) {
        DownloadFileFromURL download = new DownloadFileFromURL(true,getFilesDir().toString(),ddlForMap);
        download.addObserver(this);
        download.execute(url, "", filename);
    }

    /**
     * Renvoie des noms de fichier
     * @param finDeFichier
     * @return
     */
    private ArrayList<String> getFileNameFromRootFinishingBy(String finDeFichier) {
        ArrayList<String> xmlFileNameFromRoot = new ArrayList<String>();
        File repertoire = getFilesDir();
        int i;
        String[] listefichiers;
        listefichiers = repertoire.list();
        i = listefichiers.length;

        for (i = 0; i < listefichiers.length; i++) {
            if (listefichiers[i].endsWith(finDeFichier)) {
                xmlFileNameFromRoot.add(listefichiers[i]);
                Log.w("nom fichier xml : ", listefichiers[i].substring(0, listefichiers[i].length() - 4));// on choisit la sous chaine - les 5 derniers caracteres ".java"
            }
        }
        return xmlFileNameFromRoot;
    }

    /**
     * Detruit un fichier temporaire
     */
    public void destroyFichierTMP(){
        ArrayList<String> xmlFileNameFromRootArray = getFileNameFromRootFinishingBy("tmp.xml");
        for (int k=0 ; k < xmlFileNameFromRootArray.size() ; k++) {
            String xmlFileNameFromRoot = getFilesDir().toString() + '/' + xmlFileNameFromRootArray.get(k);
            File file = new File(xmlFileNameFromRoot);
            file.delete();
        }
        displayMap();
    }
}