/**
 * This file is part of Pyxidia.
 *
 * Pyxidia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Pyxidia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Pyxidia.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright, The Pyxidia team (1), ENSICAEN, 6 Boulevard Marechal Juin 14000 Caen, 2017
 *
 * (1) The Pyxidia team:
 * Denis Chen - ENSICAEN Informatique MSI - 2015/2016 - dchen@ecole.ensicaen.fr
 * Annick Volcy - ENSICAEN Informatique MSI - 2015/2016 - volcy@ecole.ensicaen.fr
 * Florentin Blanc - ENSICAEN Informatique MSI - 2015/2016 - fblanc@ecole.ensicaen.fr
 * Tristan Fauquette - ENSICAEN Informatique MSI - 2015/2016 - fauquette@ecole.ensicaen.fr
 * Vincent Duplessis - ENSICAEN Informatique Image - 2016/2017 - duplessis@ecole.ensicaen.fr
 * Benjamin Houx - ENSICAEN Informatique Image - 2016/2017 - houx@ecole.ensicaen.fr
 * Axel Ollivier - ENSICAEN Informatique MSI - 2016/2017 - ollivier@ecole.ensicaen.fr
 * Yann Pellegrini - ENSICAEN Informatique Image - 2016/2017 - ypellegrini@ecole.ensicaen.fr
 * Elodie Proux - ENSICAEN Informatique MSI - 2016/2017 - proux@ecole.ensicaen.fr
 *
 */

package com.projet2a.projetcourseorientation.secondaryActivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import com.projet2a.projetcourseorientation.MainActivity;
import com.projet2a.projetcourseorientation.R;
import com.projet2a.projetcourseorientation.modele.Course;


/**
 * Classe utilisee pour l'affichage des scores.
 */

public class ScoresDisplayerActivity extends AppCompatActivity {
    public final static int SHOW_SCORES_MENU = 3;
    private static final String TAG = MainActivity.class.getSimpleName();

    public  boolean BOOLEAN_SIMPLE_SCORE=false;


    SharedPreferences _spScore;
    SharedPreferences _spTaille;

    private Course _course;

    /**
     * Methode appele a la creation.
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores_displayer);

        _course = getIntent().getExtras().getParcelable("COURSE");
        _course.getScore().set_activity(this);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        _spScore = this.getSharedPreferences("PrefScores", 0);
        _spTaille = this.getSharedPreferences("PrefTaille", 0);

        Bundle booleans = getIntent().getExtras();

        if(BOOLEAN_SIMPLE_SCORE= booleans.getBoolean("BOOLEAN_SIMPLE_SCORE")){
            BOOLEAN_SIMPLE_SCORE=false;
            showSimpleScore();
        }
    }

    /**
     * Affiche le score dans la vue
     * @param v
     */
    public void showDetailedScore(View v){
        Button effacer = (Button) findViewById(R.id.clearScore);
        effacer.setVisibility(View.INVISIBLE);
        TableLayout table = (TableLayout) findViewById(R.id.scoreTab);
        table.setVisibility(View.VISIBLE);
        TextView finDeCourse = (TextView) findViewById(R.id.finDeCourse);
        finDeCourse.setVisibility(View.INVISIBLE);
        TextView results = (TextView) findViewById(R.id.results);
        results.setVisibility(View.INVISIBLE);
        Button showDetailed = (Button) findViewById(R.id.ButtondetailedScore);
        showDetailed.setVisibility(View.INVISIBLE);



        _course.getScore().showDetailedScore(table);
    }

    /**
     * Montre le temps total (score simple)
     */
    public void showSimpleScore(){

        Button effacer = (Button) findViewById(R.id.clearScore);
        effacer.setVisibility(View.GONE);
        TableLayout table = (TableLayout) findViewById(R.id.scoreTab);
        table.setVisibility(View.INVISIBLE);
        TextView finDeCourse = (TextView) findViewById(R.id.finDeCourse);
        finDeCourse.setVisibility(View.VISIBLE);
        TextView results = (TextView) findViewById(R.id.results);
        results.setVisibility(View.VISIBLE);
        Button showDetailed = (Button) findViewById(R.id.ButtondetailedScore);

        if(_course.isSuccess()){
            showDetailed.setVisibility(View.VISIBLE);
            saveScore();
            _course.getScore().showSimpleScore(results, getString(R.string.runSuccess));
        } else{
            showDetailed.setVisibility(View.INVISIBLE);
            _course.getScore().showSimpleScore(results, getString(R.string.runFail));
        }
    }

    /**
     * Permet de retourner au menu
     * @param v
     */
    public void returnMenu(View v){
        Intent result = new Intent();
        setResult(RESULT_CANCELED, result);
        finish();
    }

    /**
     * Sauvegarde le score
     */
    public void saveScore(){
        String name = _course.getName();

        SharedPreferences.Editor editScore = _spScore.edit();
        SharedPreferences.Editor editTaille = _spTaille.edit();

        int size = _spTaille.getInt(name + "_taille", 0);

        size++;
        editTaille.putInt(name + "_taille", size);
        editTaille.commit();

        editScore.putInt(name + "_size", size);
        editScore.putLong(name + size, _course.getScore().getTemps());
        editScore.commit();
    }
}