var express = require('express');
var config = require('./config.json')
var app = express();
var path = require('path');
var multer  = require('multer');
var fs = require('fs');
var filenameMulter = function (req, file, cb) {

	var getFileExt = function(fileName){
		var fileExt = fileName.split(".");
		if( fileExt.length === 1 || ( fileExt[0] === "" && fileExt.length === 2 ) ) {
			return "";
		}
		return fileExt.pop();
	}
	cb(null, Date.now() + '.' + getFileExt(file.originalname))
};

app.use(function(req, res, next) {
	res.set({'Content-Type': 'text/plain; charset=utf-8;',
		'Access-Control-Allow-Origin': '*', 
		'Access-Control-Allow-Methods': 'GET, POST, DELETE, OPTIONS', 
		'Access-Control-Allow-Headers': 'X-PINGOTHER', 
		'Access-Control-Max-Age': '1728000'});
	next();
});

app.get('/', function(req, res) {
	res.setHeader('Content-Type', 'text/html; charset=utf-8;');
	res.write('<h3>API : Course d\'orientation</h3>');
	res.write('<a href="/documentation">Documentation</a>');
	res.end();
});

require('./routes/parcours')(app, multer, filenameMulter, path, config, fs);
require('./routes/course')(app, multer, filenameMulter, path, config, fs);
require('./routes/documentation')(app);

app.listen(config.port);
