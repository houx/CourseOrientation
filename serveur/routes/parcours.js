var database = require('../database/utils');

module.exports = function (app, multer, filenameMulter, path, config, fs) {

	var crypto = require('crypto');
	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, 'cartes/')
		},

		filename: filenameMulter
	});
	var upload = multer({ storage: storage, 
		fileFilter: function (req, file, cb) {
			if ( path.extname(file.originalname) !== '.xml' && path.extname(file.originalname) !== '.jpg') {
				return cb(null, false)
			}

			cb(null, true)
		} 
	});


	var checkUserData =  function(req, res, treatment){
		var login = req.body.login;
		var pass = req.body.pass;

		if (typeof login !== 'undefined' && login !== null && typeof pass !== 'undefined' && pass !== null){
			const hash = crypto.createHmac('sha256', config.sel).update(pass).digest('hex');
			database.make_request(req, res, "SELECT * FROM administrateur WHERE ad_login = ?", function(rows){
				if(rows.length == 0){
					res.end("No such user");
					return;
				}
				if(rows[0].ad_pass != hash){
					res.end("Wrong pass")
					return;
				}else{
					treatment();
				}
			}, [login]);
		} else{
			res.end(login + " " + pass);
		}

	}

	app.route('/parcours')
	.get(function(req, res) {
		database.make_request(req, res, "SELECT * FROM parcours", function(rows){
			res.end(JSON.stringify(rows));
		});
	})
	.post(upload.any(), function(req, res) {
		checkUserData(req, res, function(){
			var nom = req.body.nom;
			var nbBalises = req.body.nbBalises;

			var xml;
			var image;
			req.files.forEach(function(file){
				if(file.fieldname == "xml"){
					xml = file;
				} else if (file.fieldname == "image"){
					image = file;
				}
			})

			if (typeof nom !== 'undefined' && nom !== null 
				&& typeof xml !== 'undefined' && xml !== null
				&& typeof image !== 'undefined' && image !== null
				&& typeof nbBalises !== 'undefined' && nbBalises !== null){
				database.make_request(req, res, "INSERT INTO parcours (pa_nom, pa_xml, pa_image, pa_nb_balises) VALUES(?, ?, ?, ?)", function(rows){
					res.end("done");
				}, [nom, xml.filename, image.filename, nbBalises], function(){
					fs.unlinkSync(path.join(__dirname, "../cartes", xml.filename));
					fs.unlinkSync(path.join(__dirname, "../cartes", image.filename));
					res.status(406);
					res.end("Error !")
				});
		}
		else{
			res.end("Not enough parameters.");
		}
	});
	});

	app.get('/parcours/:nomParcours', function(req, res) {
		database.make_request(req, res, "SELECT * FROM parcours WHERE pa_nom = ?", function(rows){
			rows.forEach(function(element){
				res.write(JSON.stringify(element) + "\n");
			}, [req.params.nomParcours]);
			res.end();
		})
	});

/*    app.get('/parcours/:idParcours', function(req, res) {
        if(is_int(req.params.idParcours)){
            database.make_request(req, res, "SELECT * FROM parcours WHERE pa_id = ?", function(rows){
                rows.forEach(function(element){
                    res.write(JSON.stringify(element) + "\n");
                }, [req.params.idParcours]);
                res.end();
            })
        }else{
            res.end("[]");
        }
    });*/

    app.get('/parcours/:idParcours/compte', function(req, res) {
    	database.make_request(req, res, "SELECT count(co_id) as compte FROM course WHERE pa_id = ?", function(rows){
    		rows.forEach(function(element){
    			res.write(JSON.stringify(element.compte));
    		}, [req.params.idParcours]);
    		res.end();
    	})
    });

    app.get('/parcours/:idParcours/xml/download', function(req, res) {
    	database.make_request(req, res, "SELECT pa_xml FROM parcours WHERE pa_id = ?", function(rows){
    		rows.forEach(function(element){
    			res.download(path.join(__dirname, "../cartes", element.pa_xml), element.pa_xml, function(err){
    				if (err) {
    					console.log(err);
    				}
    			});
    		})
    	}, [req.params.idParcours]);
    });

    app.get('/parcours/:idParcours/image/download', function(req, res) {
    	database.make_request(req, res, "SELECT pa_image FROM parcours WHERE pa_id = ?", function(rows){
    		rows.forEach(function(element){
    			res.download(path.join(__dirname, "../cartes", element.pa_image), element.pa_image, function(err){
    				if (err) {
    					console.log(err);
    				}
    			});
    		})
    	}, [req.params.idParcours]);
    });

    app.get('/parcours/:idParcours/xml', function(req, res) {
    	database.make_request(req, res, "SELECT pa_xml FROM parcours WHERE pa_id = ?", function(rows){
    		rows.forEach(function(element){
    			res.sendFile(path.join(__dirname, "../cartes", element.pa_xml));
    		});
    	}, [req.params.idParcours]);
    });
}