module.exports = function (app) {
	app.get('/documentation', function(req, res) {
		res.setHeader('Content-Type', 'text/html; charset=utf-8;');
		res.write('<h3>Requêtes sur parcours :</h3>');

		res.write('<h4>Requêtes GET :</h4>');
		res.write('<ul><li><i>/parcours</i> : récupération de tous les parcours</li>');
		res.write('<li><i>/parcours/:nomParcours</i> : récupération du parcours de nom "nomParcours"</li>');
		res.write('<li><i>/parcours/:idParcours/compte</i> : récupération du nombre de courses liées au parcours d\'id : "idParcours"</li>');
		res.write('<li><i>/parcours/:idParcours/xml/download</i> : téléchargement du XML du parcours d\'id : "idParcours"</li>');
		res.write('<li><i>/parcours/:idParcours/image/download</i> : téléchargement de l\'image de la carte du parcours d\'id : "idParcours"</li>');
		res.write('<li><i>/parcours/:idParcours/xml</i> : récupération du XML du parcours d\'id : "idParcours" au format texte</li></ul>');

		res.write('<h4>Requêtes POST :</h4>');
		res.write('<ul><li><i>/parcours</i> : ajoute un parcours. Paramètres = login, pass, nom, nbBalises, xml (fichier XML), image (fichier JPG)</li></ul>');


		res.write('<h3>Requêtes sur courses :</h3>');

		res.write('<h4>Requêtes GET :</h4>');
		res.write('<ul><li><i>/courses</i> : récupération de toutes les courses</li>');
		res.write('<li><i>/courses/participant/:nomParticipant</i> : récupération de(s) courses du participant "nomParticipant"</li>');
		res.write('<li><i>/course/:idCourse</i> : récupération de la course d\'identifiant "idCourse"</li>');
		res.write('<li><i>/courses/parcours/:parcoursId</i> : récupération des courses du parcours "parcoursId"</li>');
		res.write('<li><i>/courses/classement/:parcoursId</i> : récupération des courses du parcours "parcoursId", classées du meilleur au moins bon temps</li>');
		res.write('<li><i>/courses/mode/:mode</i> : récupération des courses du mode "mode"</li>');
		res.write('<li><i>/course/:idCourse/temps</i> : récupération du temps de la course d\'identifiant "idCourse"</li>');
		res.write('<li><i>/course/:idCourse/pm</i> : récupération de(s) poste(s) manquant(s) de la course d\'identifiant "idCourse"</li>');
		res.write('<li><i>/course/:idCourse/trace/download</i> : téléchargement de la trace GPS de la course d\'identifiant "idCourse"</li>');
		res.write('<li><i>/course/:idCourse/trace</i> : récupération de la trace GPS de la course d\'identifiant "idCourse" au format texte</li></ul>');

		res.write('<h4>Requêtes POST :</h4>');
		res.write('<ul><li><i>/course</i> : ajoute une course. Paramètres = pa_id, pseudo, mode ("sportif" ou "famille")');
		res.write(', temps, trace_gps (fichier GPX), est_classe ("O" ou "N"), sous_temps (tableau JSON d\'objets contenant : balise_1, balise_2, temps)</li></ul>');
		
		res.write('<h4>Requêtes DELETE :</h4>');
		res.write('<ul><li><i>/course/:idCourse</i> : suppression de la course d\'identifiant "idCourse"</li></ul>');
		res.end();
	});
};