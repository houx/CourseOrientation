var database = require('../database/utils');

function is_int(value){
	if((parseFloat(value) == parseInt(value)) && !isNaN(value)){ 
		return true;
	} else {
		return false;
	}
}

module.exports = function (app, multer, filenameMulter, path, config, fs) {

	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, 'traces/')
		},

		filename: filenameMulter
	});

	var upload = multer({ storage: storage, 
		fileFilter: function (req, file, cb) {
			if ( path.extname(file.originalname) !== '.gpx') {
				return cb(null, false);
			}
			return cb(null, true);
		} 
	});
	var crypto = require('crypto');

	app.get('/courses', function(req, res) {
		database.make_request(req, res, "SELECT * FROM course", function(rows){
			res.end(JSON.stringify(rows));
		})
	});

	app.get('/courses/participant/:nomParticipant', function(req, res) {
		database.make_request(req, res, "SELECT * FROM course WHERE co_nom_participant = ?", function(rows){
			res.end(JSON.stringify(rows));
		}, [req.params.nomParticipant]);
	});

	app.route('/course/:idCourse')
	.get(function(req, res) {
		if(is_int(req.params.idCourse)){
			var sql = "SELECT * FROM course WHERE co_id = ?";
			database.make_request(req, res, sql, function(rows){
				rows.forEach(function(element){
					res.write(JSON.stringify(element) + "\n")
				});
				res.end();
			}, [req.params.idCourse]);
		}else{
			res.end('[]');
		}
	})
	.delete(function(req,res){
		if(is_int(req.params.idCourse)){
			/*const hash = crypto.createHmac('sha256', config.sel)
           .update('I love cupcakes')
           .digest('hex');*/
           database.make_request(req, res, "SELECT co_trace_gps FROM course WHERE co_id = ?", function(rows){
	           	rows.forEach(function(element){
	           		fs.unlinkSync(path.join(__dirname, "../traces", element.co_trace_gps));
	           		database.make_request(req, res, "DELETE FROM course WHERE co_id = ?", function(rows){
	           			res.end("DELETED");
	           		}, [req.params.idCourse]);
           		});
            });
        }else{
       		res.end('[]');
        }
    });

	app.get('/courses/parcours/:parcoursId', function(req, res) {
		if(is_int(req.params.parcoursId)){
			var sql = "SELECT * FROM course WHERE pa_id = ?"; 
			database.make_request(req, res, sql, function(rows){
				res.end(JSON.stringify(rows));
			}, [req.params.parcoursId]);
		}else{
			res.end('[]');
		}
	});

	app.get('/courses/classement/:parcoursId', function(req, res) {
		if(is_int(req.params.parcoursId)){
			var sql = "SELECT * FROM course WHERE co_est_classe = 'O' AND pa_id = ? ORDER BY co_temps_total";
			database.make_request(req, res, sql, function(rows){
				res.end(JSON.stringify(rows));
			}, [req.params.parcoursId]);
		}else{
			res.end('[]');
		}
	});

	app.get('/courses/mode/:mode', function(req, res) {
		database.make_request(req, res, "SELECT * FROM course WHERE co_mode = ?", function(rows){
			res.end(JSON.stringify(rows));
		}, [req.params.mode]);
	});


	app.get('/course/:idCourse/temps', function(req, res) {
		if(is_int(req.params.idCourse)){
			database.make_request(req, res, "SELECT * FROM sous_temps WHERE co_id = ?" , function(rows){
				res.write(JSON.stringify(rows))
				res.end();
			}, [req.params.idCourse]);
		}else{
			res.end('[]');
		}
	});

	app.get('/course/:idCourse/pm', function(req, res) {
		if(is_int(req.params.idCourse)){
			database.make_request(req, res, "SELECT * FROM poste_manquant WHERE co_id = ?", function(rows){
				res.write(JSON.stringify(rows))
				res.end();
			}, [req.params.idCourse]);
		}else{
			res.end('[]');
		}
	});

	app.get('/course/:idCourse/trace/download', function(req, res) {
		if(is_int(req.params.idCourse)){
			database.make_request(req, res, "SELECT co_trace_gps FROM course WHERE co_id = ?", function(rows){
				rows.forEach(function(element){
					res.attachment(path.join(__dirname, "../traces", element.co_trace_gps));
					res.end()
				})
			}, [req.params.idCourse]);
		}else{
			res.end('[]');
		}
	});

	app.get('/course/:idCourse/trace', function(req, res) {
		if(is_int(req.params.idCourse)){
			database.make_request(req, res, "SELECT co_trace_gps FROM course WHERE co_id = ?", function(rows){
				rows.forEach(function(element){
					res.sendFile(path.join(__dirname, "../traces", element.co_trace_gps));
					//res.end()
				})
			}, [req.params.idCourse]);
		}else{
			res.end('[]');
		}
	});

	app.post('/course', upload.single('trace_gps'), function(req, res) {
		var pa_id = req.body.pa_id;
		var pseudo = req.body.pseudo;
		var mode = req.body.mode;
		var temps = req.body.temps;
		var trace_gps = req.file;
		var est_classe = req.body.est_classe;
		var sous_temps = JSON.parse(req.body.sous_temps);

		if (typeof pa_id !== 'undefined' && pa_id !== null 
			&& typeof pseudo !== 'undefined' && pseudo !== null
			&& typeof mode !== 'undefined' && mode !== null
			&& typeof temps !== 'undefined' && temps !== null
			&& typeof trace_gps !== 'undefined' && trace_gps !== null
			&& typeof est_classe !== 'undefined' && est_classe !== null
			&& typeof sous_temps !== 'undefined' && sous_temps !== null){

			str = "INSERT INTO course (pa_id, co_mode, co_nom_participant, co_temps_total, co_est_classe, co_trace_gps) ";
		str += "VALUES (?, ?, ?, ?, ?, ?)";

		database.make_request(req, res, str, function(rows){
			sous_temps.forEach(function(temps){
				str = "INSERT INTO sous_temps (co_id, te_balise_dep, te_balise_fin, te_temps) ";
				str+= "VALUES (?, ?, ?, ?)";
				database.make_request(req, res, str, function(rows){

				}, [rows.insertId, temps.balise_1, temps.balise_2, temps.temps]);
			});
			res.end();
		}, [pa_id, mode, pseudo, temps, est_classe, trace_gps.filename]);
	}
});
}