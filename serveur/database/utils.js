var mysql = require('mysql')
var config = require('./config.json')
var pool      =    mysql.createPool({
  connectionLimit : config.connectionLimit, //important
  host     : config.host,
  user     : config.user,
  //port 	   : config.port,
  password : config.password,
  database : config.database,
  debug    : config.debug
});

exports.make_request = function (req,res,sql,treatment, preparedTab, errorTreatment) {
   
    pool.getConnection(function(err,connection){
        if (err) {
          res.status(406);
          res.end('Not Acceptable');
          console.log(err);
          return;
        }  

        console.log(Date() + ' : connected as id ' + connection.threadId);
       
        connection.query(sql, preparedTab, function(err,rows){
            connection.release();
            if(err){
              console.log(Date() + " - Database Error :\n" + err);
              errorTreatment();
            }
            if(!err) {
            	treatment(rows)
            }          
        });

        connection.on('error', function(err) {      
              res.status(406);
              res.end('Not Acceptable');
              return;    
        });
  });
}
