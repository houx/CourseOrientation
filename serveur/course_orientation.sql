-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 16 Janvier 2017 à 20:35
-- Version du serveur :  10.1.19-MariaDB
-- Version de PHP :  7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `course_orientation`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

CREATE TABLE `administrateur` (
  `ad_id` int(11) NOT NULL,
  `ad_login` varchar(30) NOT NULL,
  `ad_pass` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `course`
--

CREATE TABLE `course` (
  `co_id` int(11) NOT NULL,
  `pa_id` int(11) NOT NULL,
  `co_mode` varchar(7) NOT NULL DEFAULT 'famille',
  `co_nom_participant` varchar(50) NOT NULL,
  `co_temps_total` int(11) NOT NULL,
  `co_est_classe` varchar(1) NOT NULL DEFAULT 'N',
  `co_trace_gps` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `parcours`
--

CREATE TABLE `parcours` (
  `pa_id` int(11) NOT NULL,
  `pa_nom` varchar(30) NOT NULL,
  `pa_xml` varchar(200) NOT NULL,
  `pa_image` varchar(200) NOT NULL,
  `pa_nb_balises` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `poste_manquant`
--

CREATE TABLE `poste_manquant` (
  `pm_id` int(11) NOT NULL,
  `co_id` int(11) NOT NULL,
  `pm_balise` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `sous_temps`
--

CREATE TABLE `sous_temps` (
  `st_id` int(11) NOT NULL,
  `co_id` int(11) NOT NULL,
  `te_balise_dep` varchar(5) NOT NULL,
  `te_balise_fin` varchar(5) NOT NULL,
  `te_temps` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`ad_id`);

--
-- Index pour la table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`co_id`);

--
-- Index pour la table `parcours`
--
ALTER TABLE `parcours`
  ADD PRIMARY KEY (`pa_id`),
  ADD UNIQUE KEY `nom_unique` (`pa_nom`);

--
-- Index pour la table `poste_manquant`
--
ALTER TABLE `poste_manquant`
  ADD PRIMARY KEY (`pm_id`);

--
-- Index pour la table `sous_temps`
--
ALTER TABLE `sous_temps`
  ADD PRIMARY KEY (`st_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `administrateur`
--
ALTER TABLE `administrateur`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `course`
--
ALTER TABLE `course`
  MODIFY `co_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `parcours`
--
ALTER TABLE `parcours`
  MODIFY `pa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `poste_manquant`
--
ALTER TABLE `poste_manquant`
  MODIFY `pm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `sous_temps`
--
ALTER TABLE `sous_temps`
  MODIFY `st_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
