var poly = [];
var map;
var trace = [];

function cacher()
{
	var div = document.getElementById('participant');
	div.style.visibility="hidden"; // pour decacher =""
}

function decacher()
{
	var div = document.getElementById('participant');
	div.style.visibility="";
}

function listerParcours() 
{
	var http = new XMLHttpRequest();
	var listeParcours = document.getElementById('listeParcours');
	var url = endpoint() + "/parcours";
	http.open("GET", url, true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.onload = function() 
	{
	    var res = JSON.parse(http.responseText);
	    for(var i = 0; i < res.length; i++) 
	    {
	    	(function(i) {
	    		var obj = res[i];
		    	var li = document.createElement('li');
		    	var a = document.createElement('a');
		    	a.href = ""
		    	a.innerHTML = obj.pa_nom;
		    	a.onclick = function(event)  // res.forEach(function(obj)
		    	{
		    		event.preventDefault();
		    		afficherCourse(obj.pa_id);
		    	}
		    	li.appendChild(a);
		    	listeParcours.appendChild(li);
	    	})(i)
	    }
	}
	http.send();
}

function rechercheParcours()
{	
	var requeteHttp = new XMLHttpRequest();
	if(requeteHttp==null)
	{
		alert("Impossible d'utiliser Ajax sur ce navigateur");
	}
	else
	{
		var listeParcours = document.getElementById('listeParcours');
		var nomParcours = document.getElementById("nomParcours").value;
		requeteHttp.open('GET', endpoint() + '/parcours/'+nomParcours, true);
		requeteHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		
		requeteHttp.onload = function()
		{
			listeParcours.innerHTML=''
			if(requeteHttp.responseText == '') 
			{
				listerParcours();
				return alert("Aucune course de ce nom !");
			}

			if(nomParcours=='')
			{
				listerParcours();
			}
			else
			{
				var obj = JSON.parse(requeteHttp.responseText);
		    	var li = document.createElement('li');
		    	var a = document.createElement('a');
		    	a.href = ""
		    	a.innerHTML = obj.pa_nom;
		    	a.onclick = function(event)  // res.forEach(function(obj)
		    	{
		    		event.preventDefault();
		    		afficherCourse(obj.pa_id);
		    	}
		    	li.appendChild(a);
		    	listeParcours.appendChild(li)
		    }
		};
		requeteHttp.send();
	}
}


function afficherCourse(idCourse)
{
	decacher();
	document.getElementById('participant').setAttribute('data-courseid', idCourse);

	var http = new XMLHttpRequest();
	listeParticipants.innerHTML=''
	var url = endpoint() + "/courses/classement/"+idCourse;
	http.open("GET", url, true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	http.onload = function() 
	{
	    var res = JSON.parse(http.responseText);
	    for(var i = 0; i < res.length; i++) 
	    {
	    	(function(i) {
	    		var obj = res[i];
		    	var li = document.createElement('li');
		    	var a = document.createElement('a');
		    	a.href = "affichageStat.php"
		    	a.innerHTML = i+1 + "- "+ obj.co_nom_participant + '  -  <a target="_blank" href="' + endpoint() + '/course/' + obj.co_id + '/trace/download">(gpx)</a>';
		    	a.onclick = function(event)  // res.forEach(function(obj)
		    	{
		    		localStorage.setItem("ligne", JSON.stringify(obj));
		    	}
		    	li.appendChild(a);
		    	listeParticipants.appendChild(li);
	    	})(i)	
	    }
	}
	http.send();
}

function rechercheParticipant()
{
	console.log('rechercheParticipant');
	var requeteHttp = new XMLHttpRequest();
	var idCourse = document.getElementById('participant').getAttribute('data-courseid', idCourse);
	if(requeteHttp==null)
	{
		alert("Impossible d'utiliser Ajax sur ce navigateur");
	}
	else
	{
		var listeParticipants = document.getElementById('listeParticipants');
		var nomParticipant = document.getElementById("nomParticipants").value;
		requeteHttp.open('GET', endpoint() + '/courses/classement/'+idCourse, true);
		requeteHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		
		requeteHttp.onload = function()
		{
			listeParticipants.innerHTML=''
			if(requeteHttp.responseText == '') 
			{
				afficherCourse(idCourse);
				return alert("Aucun participant de ce nom !");
			}

			if(nomParticipant=='')
			{
				afficherCourse(idCourse);
			}
			else
			{
				var obj = JSON.parse(requeteHttp.responseText);
		    	var li = document.createElement('li');
		    	var a = document.createElement('a');
		    	a.href = "affichageStat.php"
		    	var i = recupClassement(obj, nomParticipant);
		    	if(i==-1)
		    	{
		    		afficherCourse(idCourse);
					return alert("Aucun participant de ce nom !");
		    	}
		    	else
		    	{
			    	a.innerHTML = i+1 + "- " + obj[i].co_nom_participant;
			    	a.onclick = function(event)  // res.forEach(function(obj)
			    	{
			    		// écrire : localStorage.setItem("obj", JSON.stringify(obj[i]));
			    		localStorage.setItem("ligne", JSON.stringify(obj[i]));
			    		// lire : JSON.parse(localStorage.getItem("obj"));
			    		//window.location.href="affichageStat.php"
			    	}
			    	li.appendChild(a);
			    	listeParticipants.appendChild(li)
			    }
		    }
		};
		requeteHttp.send();
	}
}


function afficherCourseStat()
{
	var obj1 = JSON.parse(localStorage.getItem("ligne"));
	var http = new XMLHttpRequest();
	listeParticipants.innerHTML=''
	var url = endpoint() + "/courses/classement/"+obj1.pa_id;
	http.open("GET", url, true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	http.onload = function() 
	{
	    var res = JSON.parse(http.responseText);
	    for(var i = 0; i < res.length; i++) 
	    {
	    	(function(i) {
	    		var obj = res[i];
		    	var checkbox = document.createElement('input')
		    	if(obj.co_id == obj1.co_id) {
		    		checkbox.checked = true
		    	}
		    	checkbox.type='checkbox';
		    	checkbox.onclick = function() {
		    		console.log(obj.co_id)
		    		togglePath(obj.co_id)
		    	};


		    	var name = document.createTextNode(i+1 + "- "+ obj.co_nom_participant);

		    	
		    	listeParticipants.appendChild(checkbox);
		    	listeParticipants.appendChild(name);
		    	listeParticipants.appendChild(document.createElement('br'));

	    	})(i)	
	    }
	}
	http.send();
}

function rechercheParticipantStat()
{
	var requeteHttp = new XMLHttpRequest();
	var obj1 = JSON.parse(localStorage.getItem("ligne"));
	if(requeteHttp==null)
	{
		alert("Impossible d'utiliser Ajax sur ce navigateur");
	}
	else
	{
		var listeParticipants = document.getElementById('listeParticipants');
		var nomParticipant = document.getElementById("nomParticipants").value;
		requeteHttp.open('GET', endpoint() + '/courses/classement/'+obj1.pa_id, true);
		requeteHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		
		requeteHttp.onload = function()
		{
			listeParticipants.innerHTML=''
			if(requeteHttp.responseText == '')
			{
				afficherCourseStat();
				return alert("Aucun participant de ce nom !");
			}

			if(nomParticipant=='')
			{
				afficherCourseStat();
			}
			else
			{
				var obj = JSON.parse(requeteHttp.responseText);
		    	var li = document.createElement('li');
		    	var a = document.createElement('a');
		    	a.href = "affichageStat.php";
		    	var i = recupClassement(obj, nomParticipant);
		    	if(i==-1)
		    	{
		    		afficherCourseStat();
					return alert("Aucun participant de ce nom !");
		    	}
		    	else
		    	{
			    	a.innerHTML = i+1 + "- " + obj[i].co_nom_participant;
			    	a.onclick = function(event)  // res.forEach(function(obj)
			    	{
			    		ajouterTrace(idCourse)
			    		poly[i].setMap(map);
			    		
			    	}
			    	li.appendChild(a);
			    	listeParticipants.appendChild(li)
			    }
		    }
		};
		requeteHttp.send();
	}
}

function ajouterTrace(idCourse)
{
	var requeteHttp = new XMLHttpRequest();
	requeteHttp.open('GET', endpoint() + '/course/'+idCourse+'/trace', true);
	requeteHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

	requeteHttp.onload = function()
	{
		if(requeteHttp.responseText == '')
		{
			afficherCourseStat(obj1.pa_id);
			return alert("Aucun trace pour ce participant !");
		}
		else
		{
			var obj = JSON.parse(requeteHttp.responseText);
			console.log(obj)
	    	handlexml(obj);
	    }
	};
}

function recupClassement(json, nom) 
{
	for(var i = 0; i < json.length; i++) 
	{
		var ligne = json[i];
		if(ligne.co_nom_participant.toLowerCase() == nom.toLowerCase()) 
		{
			return i;
		}
	}
	return -1;
}

function afficherSousTemps()
{
	var listeTemps = document.getElementById('sousTemps');
	var obj1 = JSON.parse(localStorage.getItem("ligne"));
	var idCourse = obj1.co_id;

	var http = new XMLHttpRequest();
	var url = endpoint() + "/course/"+idCourse+"/temps";
	http.open("GET", url, true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	

	http.onload = function() 
	{
	    var res = JSON.parse(http.responseText);
	    
	    for(var i = 0; i < res.length; i++) 
	    {
	    	(function(i) {
	    		var obj = res[i];
		    	var li = document.createElement('li');
		    	li.innerHTML = '<b>'+obj.te_balise_dep + " - "+ obj.te_balise_fin + " : </b> "  +obj.te_temps + ' s';
		    	listeTemps.appendChild(li);
	    	})(i)	
	    }
	}
	http.send();
}

function getNbCourses(callback) {
	var obj = JSON.parse(localStorage.getItem("ligne"));
	
	var url = endpoint() + "/courses/classement/" + obj.pa_id;
	var http = new XMLHttpRequest();
	http.open("GET", url, true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	http.onload = function() 
	{
		var res = [];
		var data = JSON.parse(http.responseText);
		for(var i = 0; i < data.length; i++) {
			res.push(data[i].co_id);
		}
		console.log(res);
		callback(res)
	}
	http.send();
}


function initMap() 
{

		var obj = JSON.parse(localStorage.getItem("ligne"));
		map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 49.2168885, lng: -0.3712861},
			scrollwheel: true,
			zoom: 7
		});

		getNbCourses(function(ids) {
			for(var i = 0; i < ids.length; i++) {
				var requeteHttp = new XMLHttpRequest();
				requeteHttp.open('GET', endpoint() + '/course/'+(ids[i])+'/trace', true);
				requeteHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				requeteHttp.co_id = ids[i]
				requeteHttp.onreadystatechange = handler
				requeteHttp.send(null);
			}
		});
		

	
}

function handler(index) 
{
		if(this.readyState == 4 && this.status == 200) {
			if(this.responseText != null) {
				handlexml(this.responseText, this.co_id);
			}
	 	}
}

function handlexml(xml, index) 
{
	var points = [];
	var bounds = new google.maps.LatLngBounds ();
		
	$(xml).find("trkpt").each(function() {
		var lat = $(this).attr("lat");
		var lon = $(this).attr("lon");
		var p = new google.maps.LatLng(lat, lon);
		points.push(p);
		bounds.extend(p);
	});

	poly[index] = new google.maps.Polyline({
		path: points,
		strokeColor: '#'+(Math.random()*0xFFFFFF<<0).toString(16),
		strokeOpacity: .7,
		strokeWeight: 4
	});

	if(JSON.parse(localStorage.getItem("ligne")).co_id != index)
		poly[i].setMap(null);

	poly[index].setMap(map);
	map.fitBounds(bounds);
}

function togglePath(i)
{
	if (poly[i].getMap() == null)
	{
		poly[i].setMap(map);
	}
	else
	{
		poly[i].setMap(null);
	}
}