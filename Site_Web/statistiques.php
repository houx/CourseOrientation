<!DOCTYPE HTML>
<!--
	Arcana by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<link rel="icon" type="image/png" href="images/logo.png" />
		<title>Statistiques</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<script type="text/javascript" src="endpoint.js"></script>
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body onload="listerParcours();cacher()">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Logo -->
						<h1><a href="index.php" id="logo">P<em>yxida</em></a></h1>

					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="index.php">Accueil</a></li>
								<li class="current"><a href="statistiques.php">Statistiques</a></li>
								<li><a href="administrateur.php">Administrateur</a></li>
								<li><a href="contact.php">Contact</a></li>
							</ul>
						</nav>

				</div>

			<!-- Main -->
				<section class="wrapper style1">
					<div class="container">
						<div class="row 200%">
							<div class="4u 12u(narrower)">
								<div id="sidebar">

									<!-- Sidebar -->

										<section>
											<h2>Parcours</h2>
											<form method="post" action="statistiques.php" onsubmit="rechercheParcours(); return false">
											<input type="text" name="nomParcours" id="nomParcours" placeholder="Nom parcours"/>
											</form><br>
											<footer>
												<a href="#" class="button" onclick="rechercheParcours(); return false;">Chercher</a>
											</footer>
										</section>

										<section>
											<ul id="listeParcours" class="links">
												
											</ul>
										</section>

								</div>
							</div>
							<div class="8u  12u(narrower) important(narrower)" id="participant">
								<div id="content">

									<!-- Content -->

										<article>
											<header>
												<h2>Participants</h2>

												<section>
													<h3>Recherche participant</h3>
													<form method="post" action="statistiques.php" onsubmit="rechercheParticipant(); return false">
													<input type="text" name="nomParticipants" id="nomParticipants" placeholder="Pseudo"/>
													</form><br>
													<footer>
														<a href="#" class="button" onclick="rechercheParticipant(); return false;">Chercher</a>
													</footer>
												</section>

												<h3> Classement </h3>
											</header>

											<section>
												<ul id="listeParticipants" class="links">
													
												</ul>
											</section>
										</article>

								</div>
							</div>
						</div>
					</div>
				</section>

			<!-- Footer -->
				<div id="footer">
					

					<!-- Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
						</ul>

					<!-- Copyright -->
						<div class="copyright">
							<ul class="menu">
								<li>&copy; Pyxida</a></li>
							</ul>
						</div>

				</div>

		</div>

		<!-- Scripts -->
			<script type="text/javascript" src="transfert.js"></script>
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
