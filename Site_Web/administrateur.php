<!DOCTYPE HTML>
<!--
	Arcana by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<link rel="icon" type="image/png" href="images/logo.png" />
		<title>Administrateur</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<script type="text/javascript" src="endpoint.js"></script>
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Logo -->
						<h1><a href="index.php" id="logo">P<em>yxida</em></a></h1>

					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="index.php">Accueil</a></li>
								<li><a href="statistiques.php">Statistiques</a></li>
								<li class="current"><a href="administrateur.php">Administrateur</a></li>
								<li><a href="contact.php">Contact</a></li>
							</ul>
						</nav>

				</div>

			
			<!-- Footer -->
				<div id="footer">
					<div class="container">
						<div class="row">
							<section class="3u 6u$(narrower) 12u$(mobilep)">
							</section>
							<section class="6u 12u(narrower)">
								<h3>Ajouter parcours</h3>
								<form  name="addform" method="post" enctype="multipart/form-data">
									<div class="row 50%">
										<div class="6u 12u(mobilep)">
											<input type="text" name="login" id="login" placeholder="Pseudo" />
										</div>
										<div class="6u 12u(mobilep)">
											<input type="password" name="pass" id="pass" placeholder="Mot de passe" />
										</div>
									</div>
									<div class="row 50%">
										<div class="6u 12u(mobilep)">
											<input type="text" name="nom" id="nom" placeholder="Nom du parcours" />
										</div>
										<div class="6u 12u(mobilep)">
											<input type="number" name="nbBalises" id="nbBalises" placeholder="Nombre de balises" />
										</div>
									</div>
									<div class="row 50%">
										<div class="12u">
											<label for="xml"> Fichier XML : </label> <input type="file" name="xml" id="xml" />
										</div>
										<div class="12u">
											<label for="image"> Fichier JPG : </label> <input type="file" name="image" id="image" />
										</div>
									</div>
									<div class="row 50%">
										<div class="12u">
											<ul class="actions">
												<li><input type="submit" class="button alt" value="Ajouter parcours" /></li>
											</ul>
										</div>
									</div>
								</form>

								

							</section>
						</div>
					</div>
					

					<!-- Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
						</ul>

					<!-- Copyright -->
						<div class="copyright">
							<ul class="menu">
								<li>&copy; Pyxida</li>
							</ul>
						</div>

				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script type="text/javascript">
				$("form[name=addform]").submit(function(event){
 
					  //disable the default form submission
					  event.preventDefault();
					 
					  //grab all form data  
					  var formData = new FormData($(this)[0]);
					 
					  $.ajax({
					    url: endpoint() + '/parcours/',
					    type: 'POST',
					    data: formData,
					    async: false,
					    cache: false,
					    contentType: false,
					    processData: false,
					    success: function (returndata) {
					    	if(returndata == "Wrong pass") {
					    		alert("wrong password");
					    	} else if(returndata == "No such user")
					    	{
					    		alert("no such user");
					    	} else {
					    		 alert("Carte ajoutée avec succès");
					    		  window.location.href="index.php"
					    	}
					     
					    }
					  });
					 
					  return false;
				});
				

			</script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>