<!DOCTYPE HTML>
<!--
	Arcana by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<link rel="icon" type="image/png" href="images/logo.png" />
		<title>Statistiques Personnel</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<script type="text/javascript" src="endpoint.js"></script>
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body onload="afficherSousTemps();afficherCourseStat();">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Logo -->
						<h1><a href="index.php" id="logo">P<em>yxida</em></a></h1>


					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="index.php">Accueil</a></li>
								<li class="current"><a href="statistiques.php">Statistiques</a></li>
								<li><a href="administrateur.php">Administrateur</a></li>
								<li><a href="contact.php">Contact</a></li>
							</ul>
						</nav>

				</div>

			<!-- Main -->
				<section class="wrapper style1">
					<div class="container">
						<div class="row 200%">
							<div class="8u 12u(narrower)">
								<div id="content">

									<!-- Content -->

									<article>
										<header>
											<h2>Pseudo</h2>
										</header>

										<h3>Traces de la course</h3>
										<p>
										

										<div id="map" style="width:800px;height:800px;"></div>
										</p>



										<h3>Splits</h3>
										<p> 
											<ul id="sousTemps" class="links">
													
											</ul>
										 </p>
									
									</article>

								</div>
							</div>
							<div class="4u 12u(narrower)">
								<div id="sidebar">

									<!-- Sidebar -->

										<section>
											<h2>Ajouter traces participants</h2>

											<section>
												<h3>Recherche participant</h3>
												<form method="post" action="affichageStat.php" onsubmit="rechercheParticipantStat(); return false">
												<input type="text" name="nomParticipants" id="nomParticipants" placeholder="Pseudo"/>
												</form><br>
												<footer>
													<a href="#" class="button" onclick="rechercheParticipantStat(); return false;">Chercher</a>
												</footer>
												<ul id="listeParticipants" class="links">
													
												</ul>
											</section>
										</section>


								</div>
							</div>
						</div>
					</div>
				</section>

			<!-- Footer -->
				<div id="footer">
					

					<!-- Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
						</ul>

					<!-- Copyright -->
						<div class="copyright">
							<ul class="menu">
								<li>&copy; Pyxida</a></li>
							</ul>
						</div>

				</div>
		</div>

		<!-- Scripts -->
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" defer></script>
			<script type="text/javascript" src="transfert.js"></script>
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPKe18UcLGePzNS3d8kzBxxycbWcROT9c&callback=initMap" async defer></script>

	</body>
</html>