<!DOCTYPE HTML>
<!--
	Arcana by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<link rel="icon" type="image/png" href="images/logo.png" />
		<title>Contact</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Logo -->
						<h1><a href="index.php" id="logo">P<em>yxida</em></a></h1>

					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="index.php">Accueil</a></li>
								<li><a href="statistiques.php">Statistiques</a></li>
								<li><a href="administrateur.php">Administrateur</a></li>
								<li class="current"><a href="contact.php">Contact</a></li>
							</ul>
						</nav>

				</div>

			
			<!-- Footer -->
				<div id="footer">
					<div class="container">
						<div class="row">
							<section class="3u 6u$(narrower) 12u$(mobilep)">
							</section>
							<section class="6u 12u(narrower)">
								<h3>Contactez-moi!</h3>
								<form  name="contactform" method="post" action="contact.php">
									<div class="row 50%">
										<div class="6u 12u(mobilep)">
											<input type="text" name="name" id="name" placeholder="Nom" />
										</div>
										<div class="6u 12u(mobilep)">
											<input type="email" name="email" id="email" placeholder="Email" />
										</div>
									</div>
									<div class="row 50%">
										<div class="12u">
											<textarea name="message" id="message" placeholder="Message" rows="5"></textarea>
										</div>
									</div>
									<div class="row 50%">
										<div class="12u">
											<ul class="actions">
												<li><input href="?email=1" type="submit" class="button alt" value="Envoyer message" /></li>
											</ul>
										</div>
									</div>
								</form>

								<?php
									if(isset($_POST['email'])) 
									{
									    $email_to = "elodie.proux.ensi@gmail.com";
									    $email_subject = "Formulaire Contact SiteWeb";
															 
									    function died($error) {
									        echo $error."<br /><br />";
									        die();
									    }
									 
									    if(!isset($_POST['name']) || !isset($_POST['email']) || !isset($_POST['message']))
									    {
									        died('We are sorry, but there appears to be a problem with the form you submitted.'); 
									    }
									 
									    $first_name = $_POST['name']; // required									 
									    $email_from = $_POST['email']; // required									 
									    $comments = $_POST['message']; // required
									 
									    $error_message = "";
									    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
									 
									    if(!preg_match($email_exp,$email_from)) 
									    { 
									   		 $error_message .= 'Email non valide.<br />';
									    }
									 
									    $string_exp = "/^[A-Za-z .'-]+$/";
									 
									    if(!preg_match($string_exp,$first_name)) 
									    {									 
									    	$error_message .= 'Nom non valide.<br />';
									    }
									 
									 
									    if(strlen($comments) < 2) 
									    {
										    $error_message .= 'Message non valide.<br />';
									    }
									 
										if(strlen($error_message) > 0) 
										{
										    died($error_message);
										}
									 
									    $email_message = "Details :\n\n";
									 
									    function clean_string($string) 
									    {
									      $bad = array("content-type","bcc:","to:","cc:","href");									 
									      return str_replace($bad,"",$string);									 
									    }					 
									     
									 
									    $email_message .= "Name: ".clean_string($first_name)."\n";	
									    $email_message .= "Email: ".clean_string($email_from)."\n";									 
									    $email_message .= "Comments: ".clean_string($comments)."\n";
									 									 
										// create email headers
										 
										$headers = 'From: '.$email_from."\r\n".
										'Reply-To: '.$email_from."\r\n" .
										'X-Mailer: PHP/' . phpversion();
										 
										@mail($email_to, $email_subject, $email_message, $headers);  
										 
									?>

									<!-- include your own success html here -->
									Merci pour votre message ! 
									 								 
									<?php									 
									}									 
									?>

							</section>
						</div>
					</div>
					


					<!-- Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
						</ul>

						
					<!-- Copyright -->

						<div class="copyright">
							<ul class="menu">
								<li>&copy; Pyxida</li>
							</ul>
						</div>

				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>